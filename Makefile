CXXFLAGS=-g `wx-config --cppflags` -I/usr/include/
LDFLAGS=`wx-config --libs` -lcurlpp -lcurl -lboost_system -lboost_filesystem
CXX=g++
CPP_FILES:= $(wildcard ordereditor/*.cpp)
OBJ_FILES= $(patsubst %.cpp,%.o,$(CPP_FILES))

all :  ordereditor

ordereditor : $(OBJ_FILES)
	$(CXX) $(CXXFLAGS) $(OBJ_FILES) $(LDFLAGS) -o working/ordereditor

# definitions files!
%.d: %.cpp
	@set -e; rm -f $@.$$$$; 
	$(CXX) -M $(CXXFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.0 $@ : ,g' < $@ > $@.$$$$; \
	include $(CPP_FILES:.cpp=.d) \
	rm -f $@.$$$$;

%.o: %.cpp, %.d
	$(CXX) $(CXXFLAGS) -c -o $@ $<;

clean:
	rm ordereditor/*.o
