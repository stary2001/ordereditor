// config.hpp : config class

// Config : A loader and storage for config.xml
class Config
{
public:
	Config(std::string filename);
	std::string& operator [](std::string s);
	void Save();
private:
	std::map<std::string,std::string> m_values;
	std::string m_filename;
};

extern Config config;