//infotypes.hpp : Infotype-related classes

//InfoTypeEntry : an entry in a list of infotypes
class InfoTypeEntry
{
public:
	std::string m_name;
	int m_number;
	int m_type;
	std::string m_data;
};

//InfoType : an infotype
class InfoType
{
public:
	InfoType() : m_external(false) {}
	~InfoType();

	std::vector<InfoTypeEntry *> m_entries;
	std::string m_name;
	int m_number;
	bool m_external;
	bool m_locked;
	void Save(OrderData::InfoType info_type,TiXmlNode *node);
};

//InfoTypeList : a list of InfoTypes
class InfoTypeList
{
public:
	~InfoTypeList();
	InfoTypeList(std::string filename);
	void Load(std::string filename);
	void Load(TiXmlNode *list);
	void Generate();
	void Save();
	std::map<OrderData::InfoType,InfoType*> m_types;
	std::string m_version;
	std::string m_filename;
	bool m_loaded;
private:
	InfoType* MakeInfoType(OrderData::InfoType type,std::string name);
};

extern InfoTypeList infotypes;