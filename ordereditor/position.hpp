//position.hpp : Position and PositionList

class Position
{
public:
	Position(int num,std::string name,std::string system,std::string loctext,std::string size,std::string design,std::string cl) 
		: m_number(num),m_name(name),m_system(system),m_loctext(loctext),m_size(size),m_design(design),m_class(cl) {};
	int m_number;
	std::string m_system;
	std::string m_loctext;
	std::string m_size;
	std::string m_design;
	std::string m_class;
	std::string m_name;
};

class PositionList
{
public:
	PositionList(std::string filename);
	~PositionList();
	std::vector<Position *> m_positions;
	bool m_loaded;
};

extern PositionList positions;