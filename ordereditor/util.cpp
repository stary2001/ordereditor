//util.hpp : utility functions, etc
#include "common.hpp"

namespace Util
{
	#if defined(WIN32) && (defined(_DEBUG)||defined(DEBUG))
	////////////////////////////////////////////////////////////////////////////////
	// Util::ShowConsole : Windows only, shows a console
	void ShowConsole()
	{
		AllocConsole();
		CONSOLE_SCREEN_BUFFER_INFO coninfo;
		int hConHandle;
		long lStdHandle;
		FILE *fp;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE),&coninfo); 
		coninfo.dwSize.Y = 500;
		SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE),coninfo.dwSize);

		lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "w" );
		*stdout = *fp;
		setvbuf( stdout, NULL, _IONBF, 0 );

		lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "r" );
		*stdin = *fp;
		setvbuf( stdout, NULL, _IONBF, 0 );

		lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
		hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
		fp = _fdopen( hConHandle, "w" );
		*stderr = *fp;
		setvbuf( stdout, NULL, _IONBF, 0 );
		std::ios::sync_with_stdio();
	}
	#else
	void ShowConsole()
	{}
	#endif

	#define DIRSEP "\\" // Change to / on linux?

	//Util::GetFileNameFromPath : Gets a filename from a path
	////////////////////////////////////////////////////////////////////////////////
	std::string GetFileNameFromPath(std::string path)
	{
		size_t last_slash=std::string::npos;
		size_t second_last_slash=0;

		last_slash=path.find(DIRSEP,0);

		while(last_slash!=std::string::npos)
		{
			second_last_slash=last_slash;
			last_slash=path.find(DIRSEP,last_slash+1);
		}

		return path.substr(second_last_slash+1);
	}

	#ifdef WIN32
		#define W64 __w64
	#else
		#define W64
	#endif

	////////////////////////////////////////////////////////////////////////////////
	// Util::PtrIsValid : Checks if a pointer is in fact, a valid pointer
	bool PtrIsValid(void *ptr)
	{
		switch((unsigned int W64)ptr)
		{
		case 0:
		case 0xBAADF00D:
		case 0xCDCDCDCD:
			return false;
		break;
		}

		return true;
	}
	////////////////////////////////////////////////////////////////////////////////
	// Util::Explode : Splits str by sep
	void Explode(std::vector<std::string> &v,std::string str,std::string sep)
	{
		for(;str.length()!=0;)
		{
			int pos=str.find(sep);
			if(pos!= std::string::npos)
			{
				int pos2=str.find(sep,pos+sep.length());
				if(pos2!= std::string::npos)
				{
					v.push_back(str.substr(0,pos));
					str=str.substr(pos+sep.length());
				}
				else
				{
					v.push_back(str.substr(0,pos));
					v.push_back(str.substr(pos+sep.length()));
					str="";
				}
			}
			else
			{
				v.push_back(str);
				str="";
			}
		}
	}
}
