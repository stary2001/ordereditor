#include "common.hpp"
#include "panel.hpp"
extern Panel *main_panel;
#include "order.hpp"
#include "turn.hpp"
#include "sentturnsdialog.hpp"

class Proxy : public wxTreeItemData
{
public:
	Proxy(Turn *ptr) { m_turn = ptr; m_list=NULL; }
	Proxy(TurnList *ptr) { m_list = ptr; m_turn = NULL; }

	Turn *m_turn;
	TurnList *m_list;
};

SentTurnsDialog::SentTurnsDialog(wxWindow *w) : wxDialog(w,wxID_ANY,"Sent Turns",wxDefaultPosition,wxSize(352,288))
{
	int id = idman.NextID();

	m_restore = new wxButton(this,id,"Restore Current",wxPoint(8,224),wxSize(132,32));
	Connect(id,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(SentTurnsDialog::OnRestore));
	m_tree = new wxTreeCtrl(this,idman.NextID(),wxPoint(8,32),wxSize(320,192));
	
	wxTreeItemId root = m_tree->AddRoot("Sent Turns");

	directory_iterator end_itr; // default construction yields past-the-end
	try
	{
		for(directory_iterator itr( "sent_turns" );itr != end_itr;++itr)
		{
			
			TurnList *list = new TurnList(itr->path().string());
			list->Load();

			m_turn_list_list.push_back(list);
			std::ostringstream os;
			os << list->m_turns.size();
			if(list->m_turns.size() == 1) { os << " turn"; } else { os << " turns"; }
			os << ", sent ";
			os << boost::posix_time::from_time_t(list->m_timestamp);
			wxTreeItemId leaf = m_tree->AppendItem(root,os.str(),-1, -1,new Proxy(list));

			for(std::vector<Turn*>::iterator it = list->m_turns.begin();it!=list->m_turns.end();it++)
			{
				os.str("");
				os << (*it)->m_orders.size();
				m_tree->AppendItem(leaf,(*it)->m_posname + " (" + os.str() + " orders)",-1, -1,new Proxy(*it));
			}
			//todo: add more!
		}
	}
	catch(boost::filesystem::filesystem_error &e) // we have no sent turns!
	{
	
	}
}

SentTurnsDialog::~SentTurnsDialog()
{
	for(std::vector<TurnList*>::iterator it=m_turn_list_list.begin();it!=m_turn_list_list.end();it++)
	{
		delete *it;
	}
}

void SentTurnsDialog::OnRestore(wxCommandEvent &unused)
{
	wxTreeItemData *sel = m_tree->GetItemData(m_tree->GetSelection());
	
	if(sel!=NULL)
	{
		Proxy *ptr = (Proxy*)sel;

		if(ptr->m_list)
		{
			TurnList *list = ptr->m_list;
			
			std::vector<Turn*> turns;

			for(std::vector<Turn*>::iterator it = list->m_turns.begin();it!=list->m_turns.end();it++)
				turns.push_back(new Turn(*(*it))); // copy the turns

			for(std::vector<Turn*>::iterator it = turns.begin();it!=turns.end();it++)
			{
				main_panel->m_turnlist->m_turns.push_back(*it);
				(*it)->m_loaded=true;
			}

			std::string filename = list->m_filename;

			main_panel->BuildList();
		}
		else
		{
			Turn *t = ptr->m_turn;
			Turn *copy=new Turn(*t);
			main_panel->m_turnlist->m_turns.push_back(copy); // copy
			copy->m_loaded=true;
			main_panel->BuildList();
		}
	}
}