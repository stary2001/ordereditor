#include "common.hpp"
#include "turndialog.hpp"
#include "infotypes.hpp"

Order::Type IndexToTypeFlag(int index)
{
	switch(index)
	{
	case 0:
		return Order::Any;
	break;

	case 1:
		return Order::Basic;
	break;

	case 2:
		return Order::Movement;
	break;
	
	case 3:
		return Order::Transaction;
	break;
	
	case 4:
		return Order::StandingOrder;
	break;

	case 5:
		return Order::Scan;
	break;

	case 6:
		return Order::Create;
	break;

	case 7:
		return Order::Turntype;
	break;

	default:
		return Order::Any;
	break;
	}
}

void TurnDialog::MakeControls()
{
	const int control_x=316;

	int id=idman.NextID();
	Connect(id,wxEVT_COMMAND_LISTBOX_SELECTED,wxCommandEventHandler(TurnDialog::OrderListSelected));
	m_orders=new wxListBox(this,id,wxPoint(16,48),wxSize(200,300));

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_LISTBOX_DOUBLECLICKED,wxCommandEventHandler(TurnDialog::CurOrderListDoubleClick));
	m_curorders=new wxListBox(this,id,wxPoint(510,8),wxSize(200,340));


	wxString turnoptions[]={"Any","Basic","Movement","Transactions","Standing Orders","Scans","Create","Turn Type"};

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHOICE_SELECTED,wxCommandEventHandler(TurnDialog::NewSelection));
	m_type_selector=new wxChoice(this,id,wxPoint(16,16),wxSize(128,24),8,turnoptions);

	m_desc=new wxTextCtrl(this,wxID_ANY,"",wxPoint(control_x-80,204),wxSize(260,148),wxTE_READONLY|wxTE_MULTILINE);
	
	std::vector<wxObjectEventFunction> textbox_func_ptrs=std::vector<wxObjectEventFunction>();
	std::vector<wxObjectEventFunction> checkbox_func_ptrs=std::vector<wxObjectEventFunction>();
	std::vector<wxObjectEventFunction> focus_func_ptrs=std::vector<wxObjectEventFunction>();

	textbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::TextInput1));
	textbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::TextInput2));
	textbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::TextInput3));
	textbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::TextInput4));
	textbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::TextInput5));

	checkbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::Check1));
	checkbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::Check2));
	checkbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::Check3));
	checkbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::Check4));
	checkbox_func_ptrs.push_back(wxCommandEventHandler(TurnDialog::Check5));
	
	focus_func_ptrs.push_back(wxFocusEventHandler(TurnDialog::FocusLost1));
	focus_func_ptrs.push_back(wxFocusEventHandler(TurnDialog::FocusLost2));
	focus_func_ptrs.push_back(wxFocusEventHandler(TurnDialog::FocusLost3));
	focus_func_ptrs.push_back(wxFocusEventHandler(TurnDialog::FocusLost4));
	focus_func_ptrs.push_back(wxFocusEventHandler(TurnDialog::FocusLost5));

	for(int i=0;i<5;i++)
	{
		id=idman.NextID();
		Connect(id,wxEVT_COMMAND_TEXT_UPDATED,textbox_func_ptrs[i]);
		
		m_textinputs[i]=new wxTextCtrl(this,id,"",wxPoint(control_x,48 + (32*i)),wxSize(48,24));
		m_textinputs[i]->Show(false);

		m_textinputs[i]->Connect(id,wxEVT_KILL_FOCUS,focus_func_ptrs[i]);
	}

	for(int i=0;i<5;i++)
		m_labels[i]=new wxStaticText(this,wxID_ANY,"",wxPoint(control_x-80,48+(i*32)),wxSize(32,24));

	
	for(int i=0;i<5;i++)
	{
		id=idman.NextID();
		Connect(id,wxEVT_COMMAND_CHOICE_SELECTED,wxCommandEventHandler(TurnDialog::UpdateOrder));
		m_choices[i]=new wxChoice(this,id,wxPoint(control_x+56,48+(i*32)),wxSize(32,24));
		m_choices[i]->Show(false);
	}

	for(int i=0;i<5;i++)
	{
		id=idman.NextID();
		Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,checkbox_func_ptrs[i]);
		m_checks[i]=new wxCheckBox(this,id,"",wxPoint(control_x,44 + (i*32)),wxSize(32,24));
		m_checks[i]->Show(false);
	}

	/*id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,wxCommandEventHandler(TurnDialog::Check1));
	m_checks[0]=new wxCheckBox(this,id,"",wxPoint(control_x,44),wxSize(32,24));
	m_checks[0]->Show(false);
	
	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,wxCommandEventHandler(TurnDialog::Check2));
	m_checks[1]=new wxCheckBox(this,id,"",wxPoint(control_x,76),wxSize(32,24));
	m_checks[1]->Show(false);

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,wxCommandEventHandler(TurnDialog::Check3));
	m_checks[2]=new wxCheckBox(this,id,"",wxPoint(control_x,108),wxSize(32,24));
	m_checks[2]->Show(false);

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,wxCommandEventHandler(TurnDialog::Check4));
	m_checks[3]=new wxCheckBox(this,id,"",wxPoint(control_x,140),wxSize(32,24));
	m_checks[3]->Show(false);

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_CHECKBOX_CLICKED,wxCommandEventHandler(TurnDialog::Check5));
	m_checks[4]=new wxCheckBox(this,id,"",wxPoint(control_x,172),wxSize(32,24));
	m_checks[4]->Show(false);*/


	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(TurnDialog::AddOrder));
	m_add=new wxButton(this,id,"Add",wxPoint(718,8),wxSize(48,32));
	
	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(TurnDialog::InsertOrder));
	m_insert=new wxButton(this,id,"Insert",wxPoint(718,40),wxSize(48,32));

	id=idman.NextID();
	Connect(id,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(TurnDialog::DeleteOrder));
	m_delete=new wxButton(this,id,"Delete",wxPoint(718,80),wxSize(48,32));

	m_showingcurorder=false;
}

void TurnDialog::GenControls()
{
	std::vector<wxString> strings;
	std::vector<void *> orders;
	std::vector<Order *>::iterator it=template_orders.m_orders.begin();

	for(;it!=template_orders.m_orders.end();it++)
	{
		if(((*it)->m_typeflag & IndexToTypeFlag(m_type_selector->GetCurrentSelection())) == IndexToTypeFlag(m_type_selector->GetCurrentSelection()))
		{
			if(((*it)->m_posflag & m_turn->m_posflag ) == m_turn->m_posflag )
			{
				strings.push_back((*it)->m_name);
				orders.push_back((*it));
			}
		}
	}

	if(strings.size()!=0) { m_orders->Set(strings.size(),&strings[0],&orders[0]); } else { m_orders->Clear(); }
	strings.clear();
	orders.clear();
	
	int i=0;

	it=m_turn->m_orders.begin();
	for(;it!=m_turn->m_orders.end();it++)
	{
		Order *torder=template_orders.FindID((*it)->m_id);
		if((torder->m_posflag & m_turn->m_posflag ) == m_turn->m_posflag )
		{
			strings.push_back(torder->m_name);
			orders.push_back((*it)->Clone());
			i++;
		}
	}

	if(strings.size()!=0) { m_curorders->Set(strings.size(),&strings[0],&orders[0]); } else { m_curorders->Clear(); }
}

void TurnDialog::NewSelection(wxCommandEvent& unused)
{
	GenControls();
}

void TurnDialog::AddOrder(wxCommandEvent &unused)
{
	if(m_orders->GetSelection()!=-1)
	{
		Order *o=static_cast<Order*>(m_orders->GetClientData(m_orders->GetSelection()))->Clone();
		for(unsigned int i=0;i<o->m_data.size();i++)
		{
			switch(o->m_data[i]->m_type)
			{
			case OrderData::Int:
			case OrderData::Double:
			case OrderData::String:
				o->m_data[i]->m_str=m_textinputs[i]->GetValue().c_str();
			break;

			case OrderData::Bool:
				o->m_data[i]->m_str=(m_checks[i]->GetValue() ? "true" : "false");
			break;
			}
		}
		m_turn->m_orders.push_back(o);
		GenControls();
	}
}

void TurnDialog::InsertOrder(wxCommandEvent &unused)
{
	if(m_curorders->GetSelection()!=-1 && m_orders->GetSelection()!=-1)
	{
		m_turn->m_orders.insert(m_turn->m_orders.begin()+m_curorders->GetSelection(),static_cast<Order*>(m_orders->GetClientData(m_orders->GetSelection())));
		GenControls();
	}
}

void TurnDialog::DeleteOrder(wxCommandEvent &unused)
{
	if(m_curorders->GetSelection()!=-1)
	{
		int sel=m_curorders->GetSelection();
		m_curorders->Delete(sel);
		m_curorders->SetSelection(sel-1);
		m_turn->m_orders.erase(m_turn->m_orders.begin()+sel);
	}
}

void TurnDialog::CurOrderListDoubleClick(wxCommandEvent & unused)
{
	m_showingcurorder=true;

	Order *o=static_cast<Order*>(m_curorders->GetClientData(m_curorders->GetSelection()));

	for(int i=0;i<5;i++)
	{
		m_labels[i]->SetLabel("");

		m_textinputs[i]->Show(false);
		m_textinputs[i]->SetSize(wxSize(48,24));
		m_textinputs[i]->ChangeValue("");

		m_choices[i]->Show(false);
		m_checks[i]->Show(false);

		m_choices[i]->Clear();
	}

	OrderMod(o);
	m_desc->SetValue(template_orders.FindID(o->m_id)->m_desc); // fill out desc

	FillOrderData(o);

	std::istringstream is;

	for(unsigned int i=0;i<o->m_data.size();i++)
	{
		wxString s=o->m_data[i]->m_str.c_str();
		m_textinputs[i]->ChangeValue(o->m_data[i]->m_str);

		int num;
		num=atoi(o->m_data[i]->m_str.c_str());
		if(num==0) { break; }
		for(unsigned int ii=0;ii<m_choices[i]->GetStrings().size();ii++)
		{
			if(static_cast<InfoTypeEntry*>(m_choices[i]->GetClientData(ii))->m_number==num)
			{
				m_choices[i]->SetSelection(ii);
				m_dontclear = true;
				wxCommandEvent ev; // make an object
				UpdateOrder(ev);
				m_dontclear = false;
				break;
			}
		}
	}
}

void TurnDialog::FillList(int index,Order *order,OrderData::InfoType itype)
{
	std::ostringstream os;
	
	std::vector<wxString> strings;
	std::vector<void *> orders;
	if(infotypes.m_types[itype]!=NULL)
	{
		std::vector<InfoTypeEntry*>& v=infotypes.m_types[itype]->m_entries;
		
		for(std::vector<InfoTypeEntry*>::iterator it=v.begin();it!=v.end();it++)
		{
			std::string s=(*it)->m_name;
			s+=" (";
			os.str("");
			os << (*it)->m_number;
			s+=os.str();
			s+=")";
			if(order->m_links.size()>0 && order->m_links[0].second==itype) // first=infotype second=linked
			{
				// find linked param 
				OrderData *param=NULL;
				for(std::vector<OrderData*>::iterator it2=order->m_data.begin();
					it2!=order->m_data.end();it2++)
				{
					if((*it2)->m_infotype==order->m_links[0].first)
					{
						param=*it2;
						break;
					}
				}

				if(param!=NULL)
				{
					std::vector<InfoTypeEntry*> & entries = infotypes.m_types[itype]->m_entries;

					std::vector<InfoTypeEntry*>::iterator it2=entries.begin();

					for(;it2!=entries.end();it2++)
					{
						if((*it2)->m_data == param->m_str) // check data, ensure a number is not added twice
						{
							std::string s2=(*it2)->m_name;
							s2+=" (";
							os.str("");
							os << (*it2)->m_number;
							s2+=os.str();
							s2+=")";
							strings.push_back(s2);
							orders.push_back((*it2));
						}
					}
					break;
				}
			}
			else
			{
				strings.push_back(s);
				orders.push_back((*it));
			}
		}
		if(strings.size()!=0)
		{
			m_choices[index]->Append(strings.size(),&strings[0],&orders[0]);
		}
	}
	else
	{
		std::cout << "No infotype set :(" << std::endl; 
	}
}
///////////////////////////////////////////////////////////////////////////////
void TurnDialog::FillOrderData(Order *order)
{
	for(unsigned int i=0;i<order->m_data.size();i++)
	{
		OrderData *order_data=order->m_data[i];

		if(m_showingcurorder) { order_data->m_name=template_orders.FindID(order->m_id)->m_data[i]->m_name; }
		
		if(order->m_data[i]->m_proxy_infotype==-1)
		{
			m_labels[i]->SetLabel( order_data->m_name.c_str() );
		}

		if(order_data->m_infotype==OrderData::NORMAL)
		{
			switch(order_data->m_type)
			{
			case OrderData::Bool:
				m_checks[i]->Show(true);
				m_checks[i]->SetValue(order_data->m_str=="true");
			break;

			case OrderData::Int:
				m_textinputs[i]->Show(true);
				m_textinputs[i]->ChangeValue(order_data->m_str);
			break;

			case OrderData::Double:
				m_textinputs[i]->Show(true);
				m_textinputs[i]->ChangeValue(order_data->m_str);
			break;

			case OrderData::String:
				m_textinputs[i]->Show(true);
				m_textinputs[i]->SetSize(wxSize(128,24));
				m_textinputs[i]->ChangeValue(order_data->m_str);
			break;
			}
		}
		else
		{
			m_textinputs[i]->Show(true);

			m_choices[i]->SetSize(wxSize(128,24));
			m_choices[i]->Show(true);
			if((order_data->m_infotype&0xFFF0)!=0xFFF0)
			{
				unsigned int cursel=m_cur_selections[i];
				if(cursel!=-1 && cursel < infotypes.m_types[order_data->m_infotype]->m_entries.size())
				{
					std::string str_data=infotypes.m_types[order_data->m_infotype]->m_entries[cursel]->m_data;
					if(str_data.find('{')!=std::string::npos)
					{
						std::vector<std::string> split;
						Util::Explode(split,str_data,",");

						for(std::vector<std::string>::iterator it=split.begin();it!=split.end();it++)
						{
							std::vector<std::string> split2;
							Util::Explode(split2,*it,":");

							std::vector<std::string> split3;
							Util::Explode(split3,split2[0],"=");

							split2[1]=split2[1].substr(0,split2[1].length()-1); // trim brackets
							split3[1]=split3[1].substr(1);

							OrderData::InfoType proxy_type = static_cast<OrderData::InfoType>(atoi(split3[0].c_str()));
							int real_type=atoi(split3[1].c_str());
							std::string name=split2[1];

							for(unsigned int j=0;j<order->m_data.size();j++)
							{
								if(order->m_data[j]->m_infotype==proxy_type)
								{
									order->m_data[j]->m_proxy_infotype=real_type;
									m_labels[j]->SetLabel(name);
								}
							}
						}
					}
				}

				FillList(i,order,order_data->m_infotype);
			}
			else if(order_data->m_proxy_infotype!=-1)
			{
				m_textinputs[i]->Show(true);

				m_choices[i]->SetSize(wxSize(128,24));
				m_choices[i]->Show(true);

				FillList(i,order,static_cast<OrderData::InfoType>(order_data->m_proxy_infotype));
			}
		}
	}
}

void TurnDialog::OrderListSelected(wxCommandEvent & unused)
{
	m_showingcurorder=false;
	for(int i=0;i<5;i++)
	{
		m_labels[i]->SetLabel("");

		m_textinputs[i]->Show(false);
		m_textinputs[i]->SetSize(wxSize(48,24));
		m_textinputs[i]->ChangeValue("");

		m_choices[i]->Show(false);
		m_checks[i]->Show(false);

		m_choices[i]->Clear();
	}
	Order *o=template_orders.FindID(static_cast<Order*>(m_orders->GetClientData(m_orders->GetSelection()))->m_id);
	OrderMod(o);
	m_desc->SetValue(o->m_desc); // fill out desc

	FillOrderData(o);
}

void TurnDialog::UpdateOrder(wxCommandEvent & unused)
{
	int old_infotypes[5];
	
	Order *o;
	if(m_showingcurorder) { o=static_cast<Order*>(m_curorders->GetClientData(m_curorders->GetSelection())); }
	else {  o=template_orders.FindID(static_cast<Order*>(m_orders->GetClientData(m_orders->GetSelection()))->m_id); }

	for(unsigned int i=0;i<5;i++)
	{
		MenuSelected(i);

		m_cur_selections[i]=m_choices[i]->GetCurrentSelection();
		if(i<o->m_data.size())
		{
 			old_infotypes[i]=o->m_data[i]->m_proxy_infotype;
		}
		m_choices[i]->Clear();
	}
	
	OrderMod(o);

	FillOrderData(o);

	for(unsigned int i=0;i<5;i++)
	{
		if(i<o->m_data.size())
		{
			if(o->m_data[i]->m_proxy_infotype==old_infotypes[i])
			{
				m_choices[i]->SetSelection(m_cur_selections[i]);
			}
			else if(!m_dontclear)
			{
				m_textinputs[i]->ChangeValue("");
			}
		}
	}
}

void TurnDialog::Check(int i)
{
	Order *o;
	if(m_showingcurorder) { o=static_cast<Order*>(m_curorders->GetClientData(m_curorders->GetSelection())); }
	else {  o=template_orders.FindID(static_cast<Order*>(m_orders->GetClientData(m_orders->GetSelection()))->m_id); }
	
	o->m_data[i]->m_str=(m_checks[i]->IsChecked() ? "true" : "false");
}

void TurnDialog::Check1(wxCommandEvent &ev)
{
	Check(0);
}

void TurnDialog::Check2(wxCommandEvent &ev)
{
	Check(1);
}

void TurnDialog::Check3(wxCommandEvent &ev)
{
	Check(2);
}

void TurnDialog::Check4(wxCommandEvent &ev)
{
	Check(3);
}

void TurnDialog::Check5(wxCommandEvent &ev)
{
	Check(4);
}

void TurnDialog::TextInput(int i)
{
	Order *o;
	if(m_showingcurorder) 
	{ 
		o=static_cast<Order*>(m_curorders->GetClientData(m_curorders->GetSelection())); 
		o->m_data[i]->m_str=m_textinputs[i]->GetValue().c_str();
	}
	
	/*if(m_showingcurorder && infotypes.m_types[o->m_data[i]->m_infotype]->m_locked) // is the infotype of the current TextInput locked?
	{
		for(unsigned int ii=0;ii<m_choices[ii]->GetStrings().size();ii++)
		{
			std::ostringstream os;
			os << ((InfoTypeEntry*)m_choices[i]->GetClientData(ii))->m_number;
			if(os.str() == m_textinputs[i]->GetValue())
			{
				o->m_data[i]->m_str=m_textinputs[i]->GetValue().c_str();
				m_choices[i]->SetSelection(ii);
				return;
			}
		}
		m_textinputs[i]->ChangeValue("");
	}*/
}

void TurnDialog::TextInput1(wxCommandEvent &ev)
{
	TextInput(0);
}

void TurnDialog::TextInput2(wxCommandEvent &ev)
{
	TextInput(1);
}

void TurnDialog::TextInput3(wxCommandEvent &ev)
{
	TextInput(2);
}

void TurnDialog::TextInput4(wxCommandEvent &ev)
{
	TextInput(3);
}

void TurnDialog::TextInput5(wxCommandEvent &ev)
{
	TextInput(4);
}

void TurnDialog::MenuSelected(int index)
{
	if(m_choices[index]->HasClientData())
	{
		Order *o;
		if(m_showingcurorder) 
		{ 
			o=static_cast<Order*>(m_curorders->GetClientData(m_curorders->GetSelection())); 
		}

		std::ostringstream os;
		if(m_choices[index]->HasClientData() && m_choices[index]->GetCurrentSelection()!=-1)
			os << static_cast<InfoTypeEntry*>(m_choices[index]->GetClientData(m_choices[index]->GetCurrentSelection()))->m_number; // get number
		
		if(os.str()!="" && !m_dontclear)
		{
			if(m_showingcurorder)
			{
				o->m_data[index]->m_str=os.str();
			}
			m_textinputs[index]->ChangeValue(os.str().c_str());
		}
	}
}

void TurnDialog::OrderMod(Order *o)
{
	int i=0;

	for(std::vector<OrderData*>::iterator it=o->m_data.begin();it!=o->m_data.end();it++)
	{
		if((*it)->m_infotype==0xFFFF)
		{
			int cursel=m_choices[i-1]->GetCurrentSelection();
			if(cursel!=-1)
			{
				(*it)->m_infotype=static_cast<OrderData::InfoType>(atoi(infotypes.m_types[(*(it-1))->m_infotype]->m_entries[cursel]->m_data.c_str()));
				/*
					Get the infotype of the data before us, look it up in the infotype data,
					use the "data" value of that as an infotype for the current data
				*/
			}
		}
		i++;
	}
}

void TurnDialog::FocusLost(int i)
{
	// s_current_turn_dialog is a workaround for trashed this pointer in focus events

	Order *o;
	if(s_current_turn_dialog->m_showingcurorder) 
	{ 
		o=static_cast<Order*>(s_current_turn_dialog->m_curorders->GetClientData(s_current_turn_dialog->m_curorders->GetSelection())); 
		o->m_data[i]->m_str=s_current_turn_dialog->m_textinputs[i]->GetValue().c_str();
	}
	else
	{
		o=static_cast<Order*>(s_current_turn_dialog->m_orders->GetClientData(s_current_turn_dialog->m_orders->GetSelection()));
	}
	
	OrderData::InfoType itype = o->m_data[i]->m_infotype;
	if((o->m_data[i]->m_infotype & 0xFFF0) == 0xFFF0) // if it's a proxy and has a proxy infotype set
	{
		if(o->m_data[i]->m_proxy_infotype !=-1)
		{
			itype=static_cast<OrderData::InfoType>(o->m_data[i]->m_proxy_infotype);
		}
		else 
		{ 
			return;
		}
	}

	if(infotypes.m_types[itype] && infotypes.m_types[itype]->m_locked) // is the infotype of the current TextInput locked?
	{
		for(unsigned int ii=0;ii<s_current_turn_dialog->m_choices[ii]->GetStrings().size();ii++)
		{
			std::ostringstream os;
			os << ((InfoTypeEntry*)s_current_turn_dialog->m_choices[i]->GetClientData(ii))->m_number;
			if(os.str() == s_current_turn_dialog->m_textinputs[i]->GetValue())
			{
				if(s_current_turn_dialog->m_showingcurorder)
				{
					o->m_data[i]->m_str=s_current_turn_dialog->m_textinputs[i]->GetValue().c_str();
				}
				s_current_turn_dialog->m_choices[i]->SetSelection(ii);
				return;
			}
		}


		std::ostringstream os;
		void *data = s_current_turn_dialog->m_choices[i]->GetClientData(s_current_turn_dialog->m_choices[i]->GetSelection()); // wxWidgets uses a void* don't hurt me
		InfoTypeEntry *entry = static_cast<InfoTypeEntry*>(data);
		os << entry->m_number;

		s_current_turn_dialog->m_textinputs[i]->ChangeValue(os.str());
		if(s_current_turn_dialog->m_showingcurorder)
		{
			o->m_data[i]->m_str="";
		}
	}
}

void TurnDialog::FocusLost1(wxFocusEvent &ev)
{
	FocusLost(0);
	ev.Skip();
}

void TurnDialog::FocusLost2(wxFocusEvent &ev)
{
	FocusLost(1);
	ev.Skip();
}

void TurnDialog::FocusLost3(wxFocusEvent &ev)
{
	FocusLost(2);
	ev.Skip();
}

void TurnDialog::FocusLost4(wxFocusEvent &ev)
{
	FocusLost(3);
	ev.Skip();
}

void TurnDialog::FocusLost5(wxFocusEvent &ev)
{
	FocusLost(4);
	ev.Skip();
}

TurnDialog *TurnDialog::s_current_turn_dialog = NULL;
