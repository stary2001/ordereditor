//turn.hpp : Turn and TurnList

//Turn : A list of orders and a position id 
class Turn
{
public:
	int m_id;
	static int m_maxid;

	std::string m_version;
	Turn() : m_version("") { m_send=false; m_id=m_maxid++; m_sequenced = false; };
	//Turn(Turn &t) { m_filename=t.m_filename; m_posid=t.m_posid; m_posflag=t.m_posflag; m_version=t.m_version; m_orders=t.m_orders; }; ??
	
	Turn(Turn &other);

	Turn(std::string filename);
	~Turn();

	std::string m_name;
	std::vector<Order * > m_orders;
	
	Order::PosType m_posflag;
	Order *FindID(int id);
	void Save(std::string filename,bool force=false);
	void Save(TiXmlNode *turn);
	void Load(TiXmlNode *data);
	std::string m_filename;
	int m_posid;
	bool m_send;

	bool m_sent;

	bool m_sequenced;
	bool m_loaded;
	int m_seqorder;
	std::string m_posname;

private:
	TiXmlDocument m_doc;
	bool m_saved;
};

//TurnList : A list of turns
class TurnList
{
public:
	TurnList(std::string f) : m_filename(f) {};
	TurnList() {};
	~TurnList();
	std::vector<Turn*> m_turns;
	std::string m_filename;
	time_duration::sec_type m_timestamp;
	void Save();
	void Load();
};


extern Turn template_orders;