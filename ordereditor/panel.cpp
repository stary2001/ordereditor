//panel.cpp : Panel class
#include "common.hpp"

class Panel;

#include "turndialog.hpp"
#include "newturndialog.hpp"
#include "infotypes.hpp"
#include "mylistctrl.hpp"
#include "panel.hpp"
#include "position.hpp"
#include "codedialog.hpp"
#include "sentturnsdialog.hpp"

// TypeToName : Converts a PosType to a name, for displaying

const char * TypeToName(Order::PosType p)
{
	switch(p)
	{
	case Order::Group:
		return "Ground Party";
	break;

	case Order::Ship:
		return "Ship";
	break;

	case Order::Starbase:
		return "Starbase";
	break;

	case Order::Political:
		return "Political";
	break;

	case Order::Platform:
		return "Platform";
	break;

	case Order::Agent:
		return "Agent";
	break;

	case Order::Debris:
		return "Debris";
	break;

	default:
		return "None";
	break;
	}
}
// Constructor
Panel::Panel(const wxString& title) : wxFrame(NULL, idman.NextID(), title, wxDefaultPosition, wxSize(764, 200))
{
	bool loaded = template_orders.m_loaded && infotypes.m_loaded && positions.m_loaded;

	m_userid = 0;
	m_turnlist=new TurnList("turns.xml");

	// Set up custom size and click handlers
	Connect(idman.m_nextid,wxEVT_SIZE,wxSizeEventHandler(Panel::OnSize));
	Connect(idman.m_nextid,wxEVT_LEFT_DOWN,wxMouseEventHandler(Panel::OnClick));

	m_panel = new wxPanel(this, -1);

	
	// Create top menu bar and buttons
	m_menu=new wxMenuBar;

	m_file=new wxMenu;
	m_turn=new wxMenu;
	m_update=new wxMenu;

	// Create menu entries
	m_file->Append(wxID_EXIT,wxT("&Quit"));
	Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(Panel::OnQuit));

	int id;
	if(loaded)
	{
		id=idman.NextID();
		Connect(id, wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(Panel::NewTurn));
		m_turn->Append(id,wxT("&New"));

		id=idman.NextID();
		Connect(id, wxEVT_COMMAND_MENU_SELECTED ,wxCommandEventHandler(Panel::LoadTurn));
		m_turn->Append(id,wxT("&Load"));

		id=idman.NextID();
		Connect(id, wxEVT_COMMAND_MENU_SELECTED ,wxCommandEventHandler(Panel::SendTurns));
		m_turn->Append(id,wxT("&Send"));

		id=idman.NextID();
		Connect(id, wxEVT_COMMAND_MENU_SELECTED ,wxCommandEventHandler(Panel::SentTurns));
		m_turn->Append(id,wxT("&View Sent"));
	}

	id=idman.NextID();
	Connect(id, wxEVT_COMMAND_MENU_SELECTED ,wxCommandEventHandler(Panel::OnCode));
	m_turn->Append(id,wxT("&Enter Code"));

	id=idman.NextID();
	Connect(id, wxEVT_COMMAND_MENU_SELECTED ,wxCommandEventHandler(Panel::OnUpdate));
	m_update->Append(id,wxT("&Update"));

	// Add buttons to menu
	m_menu->Append(m_file,wxT("&File"));
	m_menu->Append(m_turn,wxT("&Turn"));
	m_menu->Append(m_update,wxT("&Update"));

	// Show menu
	SetMenuBar(m_menu);
	
	id=idman.NextID();
	// wxID_HIGHEST is in the event table for the double click event
	m_turns = new MyListCtrl(this,wxID_HIGHEST,wxPoint(0,0),wxSize(320,144),wxLC_REPORT);
	
	// Add columns
	m_turns->InsertColumn(0,"",wxLIST_FORMAT_LEFT,0);
	m_turns->InsertColumn(1,"Type");
	m_turns->InsertColumn(2,"Name(number)");
	m_turns->InsertColumn(3,"System");
	m_turns->InsertColumn(4,"Size");
	m_turns->InsertColumn(5,"Design");
	m_turns->InsertColumn(6,"Class");
	m_turns->InsertColumn(7,"Delete",wxLIST_FORMAT_LEFT,24);
	m_turns->InsertColumn(8,"Don't Send",wxLIST_FORMAT_LEFT,24);
	m_turns->InsertColumn(9,"Sequence",wxLIST_FORMAT_LEFT,24);

	const unsigned int xsz = 72 ;
	const unsigned int ysz = 24 ;

	// make image list
	wxImageList *img_list=new wxImageList(xsz,ysz);
	
	// Bitmaps must not be selected by a DC for addition to the image list
    // a code block is used to destruct the wxMemoryDC


	wxBitmap unchecked_bmp(xsz, ysz),
             checked_bmp(xsz, ysz),
			 delete_button_bmp(xsz,ysz),
			 dot_bmp(xsz,ysz),
			 sequence_updown_bmp(xsz,ysz);

	wxBitmap up_arrow("up.png",wxBITMAP_TYPE_PNG);
	wxBitmap down_arrow("down.png",wxBITMAP_TYPE_PNG);

	// bmp("filename",wxBITMAP_TYPE_BMP)

    {
        wxMemoryDC renderer_dc;
 
        // Unchecked
        renderer_dc.SelectObject(unchecked_bmp);
        renderer_dc.SetBackground(*wxWHITE_BRUSH);
        renderer_dc.Clear();
        wxRendererNative::Get().DrawCheckBox(this, renderer_dc, wxRect(36, 0, 24, 24), 0);
 
        // Checked
        renderer_dc.SelectObject(checked_bmp);
        renderer_dc.SetBackground(*wxWHITE_BRUSH);
        renderer_dc.Clear();
        wxRendererNative::Get().DrawCheckBox(this, renderer_dc, wxRect(36, 0, 24, 24), wxCONTROL_CHECKED);
		
		// Our delete button
		renderer_dc.SelectObject(delete_button_bmp);
        renderer_dc.SetBackground(*wxWHITE_BRUSH);
        renderer_dc.Clear();
		#ifdef WIN32
			wxRendererNative::Get().DrawPushButton(this, renderer_dc, wxRect(36, 0, 24, 24));
		#else
			wxRendererNative::Get().DrawPushButton(this, renderer_dc, wxRect(20, 0, 24, 24));
		#endif
		// Up / down for sequencing
		renderer_dc.SelectObject(sequence_updown_bmp);
        renderer_dc.SetBackground(*wxWHITE_BRUSH);
        renderer_dc.Clear();
		
	    wxRendererNative::Get().DrawCheckBox(this, renderer_dc, wxRect(0, 0, 24, 24), wxCONTROL_CHECKED);
		wxRendererNative::Get().DrawPushButton(this, renderer_dc, wxRect(24, 0, 24, 24));
        wxRendererNative::Get().DrawPushButton(this, renderer_dc, wxRect(48, 0, 24, 24));

		renderer_dc.DrawBitmap(up_arrow,24,0);
		renderer_dc.DrawBitmap(down_arrow,48,0);

		renderer_dc.SelectObject(dot_bmp);
		renderer_dc.SetBackground(*wxWHITE_BRUSH);
        renderer_dc.Clear();
	}

	img_list->Add(unchecked_bmp);
	img_list->Add(checked_bmp);
	img_list->Add(delete_button_bmp);
	img_list->Add(dot_bmp);
	img_list->Add(sequence_updown_bmp);

	m_turns->AssignImageList(img_list,wxIMAGE_LIST_SMALL);

	Center();

	m_turnlist->Load();
	BuildList();

	m_initialised = true;
}

// Destructor
Panel::~Panel()
{
	delete m_panel;
	delete m_turns;
	delete m_turnlist;
}

////////////////////////////////////////////////////////////////////////////////
// Panel::LoadTurn : Handles loading a turn
void Panel::LoadTurn(wxCommandEvent &unused)
{
	wxFileDialog fd(m_panel,"Load Turn","","","*.trn");
	
	if(fd.ShowModal() == wxID_OK)
	{
		TurnDialog dlg(m_panel,std::string(fd.GetPath().c_str()));
		dlg.ShowModal();
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::NewTurn : Handles making a new turn
void Panel::NewTurn(wxCommandEvent &unused)
{
	std::ostringstream os;

	NewTurnDialog dlg(m_panel);
	if(dlg.ShowModal()==wxOK)
	{
		std::string name;
		os.str("");
		os << dlg.m_turn->m_posid;
		std::vector<InfoTypeEntry*>& v=infotypes.m_types[OrderData::POSLIST]->m_entries;
		std::vector<Position*>& v2=positions.m_positions;
		
		Position *pos=NULL;

		for(std::vector<Position*>::iterator it=v2.begin();it!=v2.end();it++)
		{
			if((*it)->m_number==dlg.m_turn->m_posid)
			{
				name=(*it)->m_name + "(" + os.str() + ")";
				pos = (*it);
				break;
			}
		}

		if(name=="")
		{
			pos = new Position(dlg.m_turn->m_posid,"","System","Loctext","Size","Design","Class");
			for(std::vector<InfoTypeEntry*>::iterator it=v.begin();it!=v.end();it++)
			{
				if((*it)->m_number==dlg.m_turn->m_posid)
				{
					name=(*it)->m_name + "(" + os.str() + ")";
					break;
				}
			}
		}

		long index = m_turns->InsertItem(m_turns->GetItemCount(),"",3);
	
		FillColumn(index,1,TypeToName(dlg.m_turn->m_posflag));
		FillColumn(index,2,name);
		FillColumn(index,3,pos->m_system);
		FillColumn(index,4,pos->m_size);
		FillColumn(index,5,pos->m_design);
		FillColumn(index,6,pos->m_class);
		
		FillColumn(index,7,2); // delete button

		m_turns->SetColumnWidth(7,wxLIST_AUTOSIZE_USEHEADER);

		if(dlg.m_turn->m_send) //checkbox is ticked if m_send == false
		{
			FillColumn(index,8,0); // off checkbox
		}
		else
		{
			FillColumn(index,8,1); // on checkbox
		}
		
		FillColumn(index,9,0);

		m_turns->SetColumnWidth(9,wxLIST_AUTOSIZE_USEHEADER);

		m_turnlist->m_turns.push_back(dlg.m_turn);
		m_turnlist->Save();
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnSize : Handle a size change
void Panel::OnSize(wxSizeEvent &ev)
{
	wxSize s=ev.GetSize();
	if(init)
	{
		m_turns->SetSize(s);
		int i=1;
		for(;i<=9;i++) // columns 1->9
		{
			m_turns->SetColumnWidth(i,wxLIST_AUTOSIZE_USEHEADER);
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnClick : Dispatch mouse events to the different handlers
void Panel::OnClick(wxMouseEvent &ev)
{
	int f=0; // flags

	long col = -1;
	
	long index=m_turns->HitTest(wxPoint(ev.GetX(),ev.GetY()),f,&col);

	// Fix for Linux & MacOS - wxwidgets only does this under windows :(
	#if !defined(WIN32)
	{ // block so locals inside here act properly
		int x = ev.GetX();
		int y = ev.GetY();
		int counter = m_turns->GetColumnWidth(0);

		for(int i = 0; i < m_turns->GetColumnCount() ; i++)
		{
			int ii = i+1;
			if(i==m_turns->GetColumnCount()-1) { ii = i; }
			if(x > counter && x < counter + m_turns->GetColumnWidth(ii))
			{
				col = i;
				break;
			}
			counter += m_turns->GetColumnWidth(i);
		}
	}
	#endif

	switch(col)
	{
	case 9:
		OnSequencingClick(index,ev.GetX(),ev.GetY());
	break;
	case 8:
		OnTurnsClick(index);
	break;
	case 7:
		OnTurnsDelete(index);
	break;
	}
}
/////////////////////////////////////////////////////////////////////////////////
// Panel::OnTurnsDelete : Handle deleting a turn
void Panel::OnTurnsDelete(int index)
{
	if(index==-1) { return; }
	int answer = wxMessageBox("Are you sure?", "Confirm", wxYES_NO, this);
    
    if (answer == wxYES)
	{
		m_turns->DeleteItem(index);
		m_turnlist->m_turns.erase(m_turnlist->m_turns.begin() + index);
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnSequencingClick : Handle a click on the sequencing controls
void Panel::OnSequencingClick(int index,int x,int y)
{
	if(index==-1){return;}
	Turn *t=m_turnlist->m_turns[index];
	
	if(!t->m_sequenced)
	{
		m_turns->SetItem(index,9,"",4);
		t->m_sequenced = true;
		t->m_seqorder = index+1;
	}
	else
	{
		int width =m_turns->GetColumnWidth(1) + m_turns->GetColumnWidth(2) + m_turns->GetColumnWidth(3)+ \
		m_turns->GetColumnWidth(4) + m_turns->GetColumnWidth(5) + m_turns->GetColumnWidth(6)+ \
		m_turns->GetColumnWidth(7) + m_turns->GetColumnWidth(8) - 4; // x is absolute, so subtract width of all columns up to this one to get a relative x

		x-=width;
		#ifdef WIN32
			const int first_button = 0;
			const int second_button = 24;
			const int third_button = 48;
		#else // todo: macos
			const int first_button = 0;
			const int second_button = 20;
			const int third_button = 44;
		#endif

		if(x >= first_button && x < second_button ) // disable sequencing 8px->32px
		{
			m_turns->SetItem(index,9,"",0);
			t->m_sequenced = false;
		}
		else if(x >= second_button && x < third_button) // up 32px->60px
		{
			Turn *t = m_turnlist->m_turns[index];
			if(index!=0)
			{
				m_turnlist->m_turns.erase(index + m_turnlist->m_turns.begin());
				m_turnlist->m_turns.insert((index - 1) + m_turnlist->m_turns.begin(),t);
				BuildList();
			}	
		}
		else // down 60px->84px(but last case so else)
		{
			Turn *t = m_turnlist->m_turns[index];
			if(index != m_turns->GetItemCount() - 1)
			{
				m_turnlist->m_turns.erase(index + m_turnlist->m_turns.begin());
				m_turnlist->m_turns.insert((index + 1) + m_turnlist->m_turns.begin(),t);
				BuildList();
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnTurnsClick : Handle a single click on the turns list
void Panel::OnTurnsClick(int index)
{
	if(index==-1){return;} // if no index is selected, return

	Turn *t=m_turnlist->m_turns[index];
	
	if(!t->m_send) // Toggle "Don't Send"
	{
		m_turns->SetItem(index,8,"",0); // Not active checkbox
		t->m_send = true;
	}
	else
	{
		m_turns->SetItem(index,8,"",1); // Active checkbox
		t->m_send = false;
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnTurnsDoubleClick : Handle a double click on the turns list

void Panel::OnTurnsDoubleClick(wxListEvent &ev)
{
	// Get turn that has been double clicked
	Turn *t=m_turnlist->m_turns[ev.GetIndex()];
	TurnDialog dlg(this,t); // open a dialog
	dlg.ShowModal();
}


class MessageDialog : public wxDialog
{
public:

	MessageDialog(wxWindow *parent,std::string title,std::string text) : wxDialog(parent,wxID_ANY,title.c_str(),wxDefaultPosition,wxSize(256,192))
	{
		m_text = new wxStaticText(this,wxID_ANY,text,wxPoint(16,16),wxSize(224,48));
		m_ok = new wxButton(this,idman.NextID(),"OK",wxPoint(96,120),wxSize(64,32));
		Connect(idman.m_nextid,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(MessageDialog::Button));
	}

	MessageDialog(const MessageDialog &other)
	{
		m_text = new wxStaticText(this,wxID_ANY,other.m_text->GetLabel(),wxPoint(16,16),wxSize(224,48));
		m_ok = new wxButton(this,idman.NextID(),"OK",wxPoint(96,120),wxSize(64,32));
		Connect(idman.m_nextid,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(MessageDialog::Button));
	}

	void Button(wxCommandEvent &unused)
	{
		Close(true);
	}

	wxStaticText *m_text;
	wxButton *m_ok;

	~MessageDialog()
	{
		delete m_text;
		delete m_ok;
	}
};

////////////////////////////////////////////////////////////////////////////////
// Panel::OnUpdate : Handle an update
void Panel::OnUpdate(wxCommandEvent &unused)
{
	curlpp::Easy req;

	std::stringstream ss;
	curlpp::options::WriteStream ws(&ss);

	std::vector<std::string> downloads;
	// Get list of downloads
	Util::Explode(downloads,config["downloads"],";");

	for(std::vector<std::string>::iterator it=downloads.begin();it!=downloads.end();it++)
	{
		std::string path = config["update_server"] + config["update_path"];
		//?a=<data_type>&sa=<thing to download>&uid=<userid>&code=<code>
		path += "?a="+ config["data_type"] +"&sa=" + (*it)+"&uid="+ config["userid"]+"&code="+config["hash"];
		req.setOpt<Url>(path); 
		req.setOpt(ws);

		std::cout << "Fetching " << *it << "." << std::endl;
		try
		{
			req.perform();

			TiXmlDocument doc;
			doc.Parse(ss.str().c_str());
			TiXmlNode *data = doc.FirstChild("data");
			if(data->FirstChild("error"))
			{
				MessageDialog dlg(this,"Error","The Nexus failed to respond to your request properly. Did you enter the correct ID/Code into Turns->Enter Code, from Personal->XML Access on the site?");
				dlg.ShowModal();
				return;
			}

			std::ofstream f(((*it)+".xml").c_str());
			f.write(ss.str().c_str(),ss.str().length());
			f.close();
			ss.str("");

			if((*it)=="info_data")
			{
				std::cout << "Generating ships list etc.." << std::endl;
				
				InfoTypeList infotypes2("info_data.xml"); // Open new infotype list
				// Generate infotypes that have to be generated
				infotypes2.Generate();
				infotypes2.Save();
			}
		}
		catch(curlpp::RuntimeError &e)
		{
			MessageDialog dlg(this,"Error",(*it) + ":" + e.what());
			dlg.ShowModal();
			return;
		}
	}

	MessageDialog dlg = MessageDialog(this,"Success","Download complete!\nPlease restart the order editor for the changes to take effect.");
	dlg.ShowModal();
}
////////////////////////////////////////////////////////////////////////////////
// Panel::SendTurns : Handle sending turns
void Panel::SendTurns(wxCommandEvent &unused)
{
	std::ostringstream os;

	wxMessageDialog d(this,"Are you sure?","Send Turns",wxYES_NO);
	if(d.ShowModal() == wxID_YES) // if we answered yes
	{

		unsigned int sz=0;
		unsigned int chunk_sz=10;

		for(std::vector<Turn*>::iterator it = m_turnlist->m_turns.begin();it!=m_turnlist->m_turns.end();it++) // Get size(in orders) of the turns
		{
			if((*it)->m_send)
			{
				sz+=(*it)->m_orders.size();
			}
		}

		TiXmlElement *data = new TiXmlElement("data");

		std::vector<std::string> final_data;
		std::vector<Turn *> chunk_to_turn;

		TiXmlNode *prevc=NULL;

		for(std::vector<Turn*>::iterator it = m_turnlist->m_turns.begin();it!=m_turnlist->m_turns.end();it++)
		{
			if((*it)->m_send)
			{
				if((*it)->m_orders.size() > chunk_sz) // if this will be split into more than 1 chunk then
				{
					unsigned int numchunks = (unsigned int)ceil((double)(*it)->m_orders.size()/(double)chunk_sz);
					// Get number of chunks by ceil(current turn's size in orders/chunk size)
					for(unsigned int i=0;i<numchunks;i++) // Loop over each chunk
					{
						TiXmlDocument doc;
						doc.LinkEndChild(new TiXmlDeclaration("1.0","iso-8859-1",""));
						TiXmlNode *n2 = new TiXmlElement("turns");
						TiXmlNode *n = new TiXmlElement("turn");
						TiXmlPrinter printer;
						printer.SetIndent( "\t" );

						Turn t(*(*it));

						if(i==0)
						{
							t.m_orders.erase(t.m_orders.begin()+chunk_sz,t.m_orders.end()); // erase all but 1st chunk_sz
						}
						else
						{
							t.m_orders.erase(t.m_orders.begin(),t.m_orders.begin()+(i*chunk_sz)); // Erase from the beginning to i*chunk_sz
							if(i!=numchunks-1) // if this is not the last chunk then
							{
								t.m_orders.erase(t.m_orders.begin()+(i*chunk_sz)+chunk_sz,t.m_orders.end()); // erase the last part!
							}
						}

						t.Save(n); // Save chunk "turn"

						n2->LinkEndChild(n);
						doc.LinkEndChild(n2); // link nodes
						
						doc.Accept(&printer);
						final_data.push_back(printer.Str()); // Turn the XML tree into a string of XML and put it on the list of chunks
						chunk_to_turn.push_back(*it);
					}
				}
				else // Small enough to fit in a chunk!
				{
					TiXmlDocument doc;
					doc.LinkEndChild(new TiXmlDeclaration("1.0","iso-8859-1",""));
					TiXmlNode *n2 = new TiXmlElement("turns");
					TiXmlNode *n = new TiXmlElement("turn");

					TiXmlPrinter printer;
					printer.SetIndent( "\t" );

					(*it)->Save(n);

					n2->LinkEndChild(n);
					doc.LinkEndChild(n2);

					doc.Accept(&printer);
					final_data.push_back(printer.Str());
					chunk_to_turn.push_back(*it);
				}
			}
		}

		curlpp::Easy req;

		std::stringstream ss;
		curlpp::options::WriteStream ws(&ss);
		
		// Build path

		std::string path = config["update_server"] + config["update_path"] + "?a="+ config["data_type"] +"&sa=send_orders"+"&uid="+ config["userid"]+"&code="+config["hash"];
		req.setOpt<Url>(path); 
		req.setOpt(ws);

		for(std::vector<std::string>::iterator it=final_data.begin();it!=final_data.end();it++) // Loop over each chunk
		{
			Turn *t = chunk_to_turn[it-final_data.begin()];
			try
			{
				req.setOpt<curlpp::options::PostFields>("data="+(*it));
				req.perform(); // Send
				std::cout << "Batch " << (it-final_data.begin())+1 << "/" << final_data.size() << std::endl;
				
				TiXmlDocument doc;
				doc.Parse(ss.str().c_str());
				TiXmlNode *data = doc.FirstChild("data");
				if(data->FirstChild("error"))
				{
					MessageDialog dlg = MessageDialog(this,"Error","The Nexus failed to respond to your request properly. Did you enter the correct ID/Code from Personal->XML Access?");
					dlg.ShowModal();
					return;
				}

				t->m_sent = true;

				std::vector<Position*>& v=positions.m_positions;

				for(std::vector<Position*>::iterator it2=v.begin();it2!=v.end();it2++)
				{
					if((*it2)->m_number==t->m_posid)
					{
						t->m_posname = (*it2)->m_name;
						break;
					}
				}
			}
			catch(curlpp::RuntimeError &e)
			{
				if(std::string(e.what()) == "couldn't connect to host") // i'm assuming english.... no other way to get a specific error!
				{
					//MessageBoxA(NULL,("Cannot connect to " + config["update_server"]).c_str(),"Error",MB_OK);
				}
			}
		}
		if(!boost::filesystem::exists("sent_turns"))
		{
			boost::filesystem::create_directory("sent_turns");
		}

		if(!boost::filesystem::is_directory("sent_turns"))
		{
			boost::filesystem::rename("sent_turns","sent_turns.renamed");
		}

		std::ostringstream os;
		os.unsetf(std::ios_base::dec);
		os.setf(std::ios_base::hex);
		ptime epoch(boost::gregorian::date(1970,1,1));
		ptime now = second_clock::universal_time();
		os << (now - epoch).total_seconds();

		TurnList *list = new TurnList("sent_turns/" + os.str() + ".xml");
		list->m_timestamp = (now - epoch).total_seconds();
		// erasing 2nd to last sets it to end()..
		for(std::vector<Turn*>::iterator it = m_turnlist->m_turns.begin();it!=m_turnlist->m_turns.end();) // this loop increments it by itself via erase()
		{
			if((*it)->m_sent)
			{
				list->m_turns.push_back(*it);
				it=m_turnlist->m_turns.erase(it);
				if(m_turnlist->m_turns.size()==0) { break; }
			}
			else
			{
				it++;
			}
		}

		list->Save();
		delete list;

		BuildList();

		/*TiXmlDocument doc;
		doc.Parse(ss.str().c_str());

		TiXmlNode *data = doc.FirstChild("data");*/
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::FillColumn : Fills a column in m_turns with a string
void Panel::FillColumn(int row,int column,std::string str)
{
	if(column!=1) // 0th column is an empty image!
	{
		m_turns->SetColumnWidth(column-1,wxLIST_AUTOSIZE_USEHEADER);
	}
	m_turns->SetItem(row,column,str.c_str());
}
// Panel::FillColumn : Fills a column in m_turns with an image
void Panel::FillColumn(int row,int column,int image)
{
	if(column!=1) // 0th column is an empty image!
	{
		m_turns->SetColumnWidth(column-1,wxLIST_AUTOSIZE_USEHEADER);
	}
	m_turns->SetItem(row,column,"",image);
}

////////////////////////////////////////////////////////////////////////////////
// Panel::BuildList : Rebuild the turns list
void Panel::BuildList()
{
	int seqorder = 0;
	std::ostringstream os;

	m_turns->DeleteAllItems();

	for(std::vector<Turn *>::iterator it = m_turnlist->m_turns.begin();it!=m_turnlist->m_turns.end();it++)
	{
		if(!(*it)->m_loaded)
		{
			break;
		}

		std::string name;
		os.str("");
		os << (*it)->m_posid;
		std::vector<Position*>& v=positions.m_positions;
		
		Position *pos=NULL;

		for(std::vector<Position*>::iterator it2=v.begin();it2!=v.end();it2++)
		{
			if((*it2)->m_number==(*it)->m_posid)
			{
				name=(*it2)->m_name + "(" + os.str() + ")";
				pos = (*it2);
				break;
			}
		}

		long index = m_turns->InsertItem(m_turns->GetItemCount(),"",3);

		// Fill turns list
		FillColumn(index,1,TypeToName((*it)->m_posflag));
		FillColumn(index,2,name);
		FillColumn(index,3,pos->m_system);
		FillColumn(index,4,pos->m_size);
		FillColumn(index,5,pos->m_design);
		FillColumn(index,6,pos->m_class);
		
		m_turns->SetColumnWidth(6,wxLIST_AUTOSIZE_USEHEADER);
		m_turns->SetItem(index,7,"",2);

		m_turns->SetColumnWidth(7,wxLIST_AUTOSIZE_USEHEADER);

		if(!(*it)->m_send) //checkbox is ticked if m_send == false
		{
			m_turns->SetItem(index,8,"",1);
		}
		else
		{
			m_turns->SetItem(index,8,"",0);
		}

		m_turns->SetColumnWidth(8,wxLIST_AUTOSIZE_USEHEADER);
		
		if((*it)->m_sequenced)
		{
			m_turns->SetItem(index,9,"",4);
			(*it)->m_seqorder = seqorder++;
		}
		else
		{
			m_turns->SetItem(index,9,"",0);
		}

		m_turns->SetColumnWidth(9,wxLIST_AUTOSIZE_USEHEADER);
	}
}
////////////////////////////////////////////////////////////////////////////////
// Panel::OnCode : Handle enter code dialog
void Panel::OnCode(wxCommandEvent &unused)
{
	m_userid = atoi(config["userid"].c_str());
	m_code = config["hash"];

	CodeDialog d(this,&m_userid,&m_code);
	d.ShowModal();
	std::ostringstream os;
	os << m_userid;
	config["userid"] = os.str();
	config["hash"] = m_code;
	config.Save();
}
////////////////////////////////////////////////////////////////////////////////
// Panel::SentTurns : Makes a SentTurnsDialog and shows it
void Panel::SentTurns(wxCommandEvent &unused)
{
	SentTurnsDialog dlg(this);
	dlg.ShowModal();
}

////////////////////////////////////////////////////////////////////////////////
// Panel::OnClose : Finishes and closes!
void Panel::OnClose(wxCloseEvent &ev)
{
	m_turnlist->Save();
	ev.Skip();
}

////////////////////////////////////////////////////////////////////////////////
// wxWidgets event table
BEGIN_EVENT_TABLE(Panel, wxFrame)
	EVT_LIST_ITEM_ACTIVATED(wxID_HIGHEST, Panel::OnTurnsDoubleClick)
	EVT_LEFT_DOWN(Panel::OnClick)
	EVT_CLOSE(Panel::OnClose)
END_EVENT_TABLE()
