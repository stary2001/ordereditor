// mylistctrl.hpp : MyListCtrl class
// MyListCtrl - My version of wxListControl to send mouse events to the panel
class MyListCtrl  : public wxListCtrl
{
public:
	MyListCtrl(Panel *panel, 
			wxWindowID id,
			const wxPoint& pos = wxDefaultPosition,
			const wxSize& size = wxDefaultSize,
			long style = wxLC_ICON,
			const wxValidator& validator = wxDefaultValidator,
			const wxString& name = wxListCtrlNameStr) : wxListCtrl((wxWindow*)panel,id,pos,size,style,validator,name),m_panel(panel) {};
	void OnClick(wxMouseEvent &ev);
	Panel *m_panel;
	DECLARE_EVENT_TABLE()
};