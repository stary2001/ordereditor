//panel.hpp : Defines the Panel class

#include <wx/popupwin.h>
class MyListCtrl;
class TurnList;

// Panel : wxWidgets window class
class Panel : public wxFrame
{
public:
	std::string m_curdir;

	Panel(const wxString& title);
	~Panel();

	void OnQuit(wxCommandEvent &unused)
	{
		Close(true);
	};

	void OnClose(wxCloseEvent &ev);

	//wxWidgets "glue"
	
	void NewTurn(wxCommandEvent &unused);
	void LoadTurn(wxCommandEvent &unused);

	void Login(wxCommandEvent &unused);
	void Logout(wxCommandEvent &unused);

	void OnSize(wxSizeEvent &ev);

	void OnClick(wxMouseEvent &ev);
	void OnTurnsDelete(int index);
	void OnTurnsClick(int index);
	void OnSequencingClick(int index,int x,int y);

	void OnTurnsDoubleClick(wxListEvent &ev);

	void OnUpdate(wxCommandEvent &unused);
	void SendTurns(wxCommandEvent &unused);
	void SentTurns(wxCommandEvent &unused);
	void OnCode(wxCommandEvent &unused);

	void BuildList();

	// menu and menu buttons
	wxMenuBar *m_menu;
	wxMenu *m_file;
	wxMenu *m_turn;
	wxMenu *m_update;

	// wxWidgets panel object
	wxPanel *m_panel;
	
	// Turns display and storage
	MyListCtrl *m_turns;
	TurnList *m_turnlist;

	// userID and code for XML access
	int m_userid;
	std::string m_code;
	
private:
	// prevent race condition
	bool m_initialised;
	void FillColumn(int row,int column,std::string str);
	void FillColumn(int row,int column,int image);

	DECLARE_EVENT_TABLE()
};