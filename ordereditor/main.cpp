// main.cpp : Main file, glues all the bits together
#include "common.hpp"

bool init  = false;

#include "main.hpp"
#include "order.hpp"
#include "turn.hpp"
#include "panel.hpp"
#include "infotypes.hpp"

#ifdef WIN32
#include <windows.h>
#endif

IMPLEMENT_APP(MyApp)

Panel *main_panel=NULL; // allow other classes to access the main panel ( mainly Turn to use as a parent for Save dialogs )
////////////////////////////////////////////////////////////////////////////////
// MyApp::OnInit : Called by wxWidgets on start
bool MyApp::OnInit()
{
	// set up cURLpp
	atexit(curlpp::terminate);
	curlpp::initialize();

	// allow loading of PNG images using wxWidgets
	wxImage::AddHandler(new wxPNGHandler);
	
	//create main panel
	Panel *panel = new Panel("Order Editor");
	main_panel = panel;
	// prevent a race condition
	init = true;
    panel->Show(true);

	// Show console if we are a debug build
	#if defined(WIN32) && (defined(_DEBUG)||defined(DEBUG))
	Util::ShowConsole();
	#endif

    return true;
}