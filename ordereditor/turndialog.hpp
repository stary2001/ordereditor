//turndialog.hpp : TurnDialog class

#include "order.hpp"
#include "turn.hpp"
/*

		ANY						=	0x0,		//
		THRUSTMOVE				=	0x1,		// 
		MOVEMENT					=	0x2,		// 
		TRANSACTION				=	0x4,		// 
		STANDINGORDER				=	0x8,		// 
		BASIC						=	0x10,		// 
		SCAN						=	0x20,		//
		OTHER						=	0x40,		// 
		ISSUE						=	0x80,		// 
		CREATE					=	0x100,		//
		SQUADRONORDER				=	0x200,		//
		SQUADRONSTANDINGORDER		=	0x400,		//
		TURNTYPE					=	0x800,		//
*/

class TurnDialog : public wxDialog
{
public:

	TurnDialog(wxWindow *w,Order::PosType p) : wxDialog(w,idman.NextID(),"Edit Turn",wxDefaultPosition,wxSize(848,384)),m_turn(new Turn())
	{
		s_current_turn_dialog=this;
		m_dontclear = false;
		m_cur_selections[0]=-1;
		m_cur_selections[1]=-1;
		m_cur_selections[2]=-1;
		m_cur_selections[3]=-1;
		m_cur_selections[4]=-1;

		m_turn->m_posflag=p;
		//Connect(idman.m_nextid,wxEVT_LEFT_DOWN,wxMouseEventHandler(TurnDialog::CurOrderListMouseDown));
		//Connect(idman.m_nextid,wxEVT_LEFT_UP,wxMouseEventHandler(TurnDialog::CurOrderListMouseUp));
		MakeControls();
		GenControls(); 
	};

	TurnDialog(wxWindow *w,std::string filename) : wxDialog(w,wxID_ANY,"Edit Turn",wxDefaultPosition,wxSize(768,384)),m_turn(new Turn(filename))
	{
		s_current_turn_dialog=this;
		m_dontclear = false;
		m_cur_selections[0]=-1;
		m_cur_selections[1]=-1;
		m_cur_selections[2]=-1;
		m_cur_selections[3]=-1;
		m_cur_selections[4]=-1;

		MakeControls();
		GenControls(); 
	};

	TurnDialog(wxWindow *w,Turn *t) : wxDialog(w,wxID_ANY,"Edit Turn",wxDefaultPosition,wxSize(768,384)),m_turn(t)
	{
		s_current_turn_dialog=this;
		m_dontclear = false;
		m_cur_selections[0]=-1;
		m_cur_selections[1]=-1;
		m_cur_selections[2]=-1;
		m_cur_selections[3]=-1;
		m_cur_selections[4]=-1;

		MakeControls();
		GenControls(); 
	};

	~TurnDialog()
	{
		s_current_turn_dialog=NULL;
		delete m_orders;
		delete m_curorders;
		delete m_type_selector;
		delete m_desc;

		delete m_add;
		delete m_insert;
		delete m_delete;
	}

	bool m_dontclear;

	//wxWidgets "glue"
	void NewSelection(wxCommandEvent & unused);
	
	void OrderListSelected(wxCommandEvent & unused);
	void CurOrderListDoubleClick(wxCommandEvent & unused);
	
	void CurOrderListMouseDown(wxMouseEvent &ev);
	void CurOrderListMouseUp(wxMouseEvent &ev);

	void AddOrder(wxCommandEvent &unused);
	void InsertOrder(wxCommandEvent &unused);
	void DeleteOrder(wxCommandEvent &unused);


	void FillOrderData(Order *order);
	void FillList(int index,Order* order,OrderData::InfoType itype);

	void UpdateOrder(wxCommandEvent & unused);

	void MenuSelected(int index);
	void Menu1Selected(wxCommandEvent &unused);
	void Menu2Selected(wxCommandEvent &unused);
	void Menu3Selected(wxCommandEvent &unused);
	void Menu4Selected(wxCommandEvent &unused);
	void Menu5Selected(wxCommandEvent &unused);

	void Check(int i);
	void Check1(wxCommandEvent &ev);
	void Check2(wxCommandEvent &ev);
	void Check3(wxCommandEvent &ev);
	void Check4(wxCommandEvent &ev);
	void Check5(wxCommandEvent &ev);

	void TextInput(int i);
	void TextInput1(wxCommandEvent &ev);
	void TextInput2(wxCommandEvent &ev);
	void TextInput3(wxCommandEvent &ev);
	void TextInput4(wxCommandEvent &ev);
	void TextInput5(wxCommandEvent &ev);

	void FocusLost(int i);
	void FocusLost1(wxFocusEvent &ev);
	void FocusLost2(wxFocusEvent &ev);
	void FocusLost3(wxFocusEvent &ev);
	void FocusLost4(wxFocusEvent &ev);
	void FocusLost5(wxFocusEvent &ev);

	void OrderMod(Order *o);
	Turn *m_turn;
private:
	void MakeControls();
	void GenControls();

	wxListBox *m_orders;
	wxListBox *m_curorders;
	wxChoice *m_type_selector;
	wxTextCtrl *m_desc;

	wxTextCtrl *m_textinputs[5];
	wxStaticText *m_labels[5];
	wxChoice *m_choices[5];
	wxCheckBox *m_checks[5];

	wxButton *m_add;
	wxButton *m_insert;
	wxButton *m_delete;

	int m_cur_selections[5];

	bool m_showingcurorder;

	static TurnDialog *s_current_turn_dialog;
};	
