// idmanager.cpp : IDManager class
// IDManager : A class with an incrementing integer for use as a wxWidgets ID
class IDManager
{
public:
	int NextID()
	{
		m_nextid++;
		return m_nextid;
	};

	int m_nextid;
};

extern IDManager idman;