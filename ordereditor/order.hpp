// order.hpp : Order related classes
// OrderData - one data field of an order
class OrderData
{
public:
	//data types
	enum Type
	{
		Int,
		Double,
		String,
		Bool,	
	};

	//InfoType types

	enum InfoType
	{
		NORMAL				=	0,	
		ITEMLIST			=	1,
		SYSTEMLIST			=	2,	
		POSLIST			=	3,
		QUADLIST			=	4,
		SECURELIST			=	5,
		ENEMYLIST			=	6,
		GROUPLIST			=	7,	
		AUTHLIST			=	8,	
		SHIPLIST			=	9,
		CBODY				=	10,
		AFFS				=	11,
		ITEMTYPE			=	12,
		TROOPTYPE			=	13,
		SOTYPE				=	14,
		FLEEOPT			=	15,
		COMPLEXTYPE		=	16,
		LONGSTRING			=	17,
		COMPLEX			=	18,
		PRODUCTION			=	19,
		TRAINING			=	20,
		TRADEGOODS			=	21,
		TARGETTYPE			=	22,
		SPREADTYPE			=	23,
		CONVERTTYPE		=	24,
		SUPPORTLIST		=	25,
		LIFEFORM			=	26,
		DAYS				=	27,
		SHIPRANKS			=	28,
		POLRANKS			=	29,
		POWERS				=	30,
		REPORTS			=	31,
		VOTETYPE			=	32,
		LEAVEAFF			=	33,
		RESTICTEDTYPE		=	34,
		GPITYPE			=	35,
		PROSPECTTYPE		=	36,
		ACQUIREDINFO		=	37,
		BOMBTYPE			=	38,
		LISTTYPE			=	39,
		GPS				=	40,
		SHIPS				=	41,
		STARBASES			=	42,
		POLITICALS			=	43,
		PLATFORMS			=	44,
		AGENTS				=	45,
		DEBRIS				=	46,
		SYSTEMPLANET		=	47,
		POSTYPE			=	48,	
		GCTACTIC			=	49,
		AGENTORDER			=	50,
		SHIPSIZE			=	51,
		ARMOURTYPE			=	52,
		SHIPTYPE			=	53,
		POSITIONTYPE		=	54,
		GENERALOPT			=	55,
		TARGETOPT			=	56,
		BATTLETYPE			=	57,
		TURNTYPE			=	58,
		MISSIONDIFFICULTY	=	59,
		MISSIONTYPE		=	60,
		POLITICALAUTH		=	61,
		OFFICERSKILLS		=	62,
		BAYITEM			=	63,
		SHIPTEMPLATES		=	64,
		UPGRADETYPE		=	65,
		
		INFO_DATA1			=	0xfffb,
		INFO_DATA0			=	0xfffc,
		GROUPPROXY			=	0xfffd,
		AUTHPROXY			=	0xfffe,
		ESDPROXY			=	0xffff,
	};
	//InfoData flags
	enum InfoDataFlag
	{
		FLAG_NONE			=	0,	
		FLAG_COMPLEX		=	0x01,
		FLAG_PRODUCTION		=	0x02,
		FLAG_LIFEFORM		=	0x04,
		FLAG_TRAINING		=	0x08,
		FLAG_TRADEGOODS		=	0x10,
		FLAG_BAY			=	0x20
	};
	
	//constructor
	OrderData(Type t,InfoType i,std::string s,std::string name) : m_type(t),m_infotype(i),m_str(s),m_name(name),m_proxy_infotype(-1) {};

	Type m_type;
	InfoType m_infotype;
	int m_proxy_infotype;

	std::string m_str;
	std::string m_name;

};

// Order - one or more OrderData and a name/number and optionally infotype links

class Order
{
public:
	// order type
	enum Type
	{
		Any					=	0x0,		// 
		ThrustMove				=	0x1,		// 
		Movement					=	0x2,		// 
		Transaction				=	0x4,		// 
		StandingOrder				=	0x8,		// 
		Basic						=	0x10,		// 
		Scan						=	0x20,		//
		Other						=	0x40,		// 
		Issue						=	0x80,		// 
		Create						=	0x100,		//
		SquadronOrder				=	0x200,		//
		SquadronStandingOrder		=	0x400,		//
		Turntype					=	0x800,		//
	};

	// position type
	enum PosType
	{
		None=0x00,
		Group=0x01,
		Ship=0x02,
		Player=0x02,
		Starbase=0x04,
		Political=0x08,
		Platform=0x10,
		Agent=0x20,
		Debris=0x40
	};

	Order(int id,
		std::string name,
		std::vector<OrderData*> &data,
		Order::Type t,Order::PosType p,
		std::vector<std::pair<int,int> > &links)
: m_id(id), m_name(name), m_data(data), m_typeflag(t), m_posflag(p), m_links(links) {};
	~Order();
	
	std::string m_name;
	int m_id;

	std::ostream & operator <<(std::ostream &o);
	Type m_typeflag;
	PosType m_posflag;

	std::vector<OrderData *> m_data;
	std::vector<std::pair<int,int> > m_links;

	Order *Clone();
	std::string m_desc;
private:
};

