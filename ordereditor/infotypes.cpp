//infotypes.cpp : infotype-related classes

#include "common.hpp"
#include "order.hpp"
#include "infotypes.hpp"
////////////////////////////////////////////////////////////////////////////////
//Construtor
InfoTypeList::InfoTypeList(std::string filename)
{
	Load(filename);
}
////////////////////////////////////////////////////////////////////////////////
// InfoTypeList::Load : Loads an infotype list from a TinyXML node
void InfoTypeList::Load(TiXmlNode *list)
{
	TiXmlNode *child = NULL;

	for(;child=list->IterateChildren(child);)
	{
		InfoType *t=new InfoType();
		t->m_name=static_cast<TiXmlElement*>(child)->Attribute("name");
		t->m_number=atoi(static_cast<TiXmlElement*>(child)->Attribute("num"));
		TiXmlNode *child2=NULL;
		t->m_locked=false;
		for(;child2=child->IterateChildren(child2);)
		{
			InfoTypeEntry *ent=new InfoTypeEntry();
			ent->m_name=static_cast<TiXmlElement*>(child2)->Attribute("name"); // fill name
			ent->m_number=atoi(static_cast<TiXmlElement*>(child2)->Attribute("num"));// fill type
			if(!child2->NoChildren())
				ent->m_data=child2->FirstChild()->Value();
			ent->m_type=atoi(static_cast<TiXmlElement*>(child2)->Attribute("type")); // fill type
			if(ent->m_name != "_locked")
			{
				t->m_entries.push_back(ent);
			}
			else
			{
				delete ent;
				t->m_locked=true;
			}
		}
		m_types[static_cast<OrderData::InfoType>(t->m_number)]=t;
	}
}
////////////////////////////////////////////////////////////////////////////////
// InfoTypeList::Load : Loads an infotype list from a file specified by filename
void InfoTypeList::Load(std::string filename)
{
	TiXmlDocument doc;
	if(!doc.LoadFile(filename))
	{
		Util::ShowConsole();
		std::cout << doc.ErrorDesc() << std::endl;
		if(doc.ErrorRow() !=0 && doc.ErrorCol()!=0)
		{
			std::cout << "line " << doc.ErrorRow() << ", column " << doc.ErrorCol() << std::endl;
		}
		//MessageBoxA(NULL,(filename + " cannot be loaded.").c_str(),"Error",MB_OK);
		//exit(1);
		m_loaded = false;
	}
	else
	{
		m_filename = filename;

		TiXmlNode *data = doc.FirstChild("data");
		m_version=data->FirstChild("version")->FirstChild()->Value();

		TiXmlNode *list= data->FirstChild("data_types");
		Load(list);
		m_loaded = true;
	}
}
////////////////////////////////////////////////////////////////////////////////
//Destructor
InfoType::~InfoType()
{
	if(!m_external)
	{
		std::vector<InfoTypeEntry*>::iterator it=m_entries.begin();
		for(;it!=m_entries.end();it++)
		{
			delete (*it);
		}
	}
	m_entries.clear();
}
////////////////////////////////////////////////////////////////////////////////
//Destructor
InfoTypeList::~InfoTypeList()
{
	std::map<OrderData::InfoType,InfoType*>::iterator it=m_types.begin();
	for(;it!=m_types.end();it++)
	{
		delete (*it).second;
	}
	m_types.clear();
}
////////////////////////////////////////////////////////////////////////////////
// InfoTypeList::MakeInfoType : Helper function for Generate
InfoType* InfoTypeList::MakeInfoType(OrderData::InfoType type,std::string name)
{
	InfoType *itype=new InfoType();
	itype->m_number = type;
	itype->m_name=name;
	itype->m_external=true;
	m_types[type] = itype;
	return itype;
}
////////////////////////////////////////////////////////////////////////////////
// InfoTypeList::Generate : Generates infotypes that are missing in the (downloaded) infotype list
void InfoTypeList::Generate()
{
	// Setup deriveved info type for phoenix
	
	InfoType *complexes=MakeInfoType(OrderData::COMPLEX,"Complexes");

	InfoType *prod=MakeInfoType(OrderData::PRODUCTION,"Production");

	InfoType *life=MakeInfoType(OrderData::LIFEFORM,"Lifeform");

	InfoType *train=MakeInfoType(OrderData::TRAINING,"Trainable");

	InfoType *trade=MakeInfoType(OrderData::TRADEGOODS,"Trade Goods");

	InfoType *bays=MakeInfoType(OrderData::BAYITEM,"Bay Items");

	// Go through items and assign to correct catagories
	for(unsigned int i=0;i<m_types[OrderData::ITEMLIST]->m_entries.size();i++)
	{
		InfoTypeEntry *info=m_types[OrderData::ITEMLIST]->m_entries[i];

		if (info->m_type & OrderData::FLAG_COMPLEX)
			complexes->m_entries.push_back(info);

		if (info->m_type & OrderData::FLAG_PRODUCTION)
			prod->m_entries.push_back(info);

		if (info->m_type & OrderData::FLAG_LIFEFORM)
			life->m_entries.push_back(info);

		if (info->m_type & OrderData::FLAG_TRAINING)	
			train->m_entries.push_back(info);

		if (info->m_type & OrderData::FLAG_TRADEGOODS)	
			trade->m_entries.push_back(info);

		if (info->m_type & OrderData::FLAG_BAY)			
			bays->m_entries.push_back(info);
	}


	// Get list for positions
	InfoType *gps=MakeInfoType(OrderData::GPS,"gps");

	InfoType *ships=MakeInfoType(OrderData::SHIPS,"Ships");

	InfoType *sbs=MakeInfoType(OrderData::STARBASES,"Starbases");

	InfoType *platforms=MakeInfoType(OrderData::PLATFORMS,"Platforms");

	InfoType *pols=MakeInfoType(OrderData::POLITICALS,"Politicals");

	InfoType *agents=MakeInfoType(OrderData::AGENTS,"Agents");

	InfoType *debris=MakeInfoType(OrderData::DEBRIS,"Debris");

	for(unsigned int i=0;i<m_types[OrderData::POSLIST]->m_entries.size();i++)
	{
		InfoTypeEntry *info=m_types[OrderData::POSLIST]->m_entries[i];
		switch(info->m_type)
		{
			case 1: gps->m_entries.push_back(info); break;
			case 2: ships->m_entries.push_back(info); break;
			case 3: sbs->m_entries.push_back(info); break;
			case 4: debris->m_entries.push_back(info); break;
			case 5: pols->m_entries.push_back(info); break;
			case 6: platforms->m_entries.push_back(info); break;
			case 7: agents->m_entries.push_back(info); break;
		}
	}

	/*
	for (var i in g_info[3])
	{
		var info=g_info[3][i];
		var pos={id:info.id,name:info.name,data:info.data,flag:0};
		switch (info.flag)	
		{
			case (1):	gps[gps.length]				=	pos;	break;
			case (2):	ships[ships.length]			=	pos;	break;
			case (3):	sbs[sbs.length]				=	pos;	break;
			case (4):	debris[debris.length]		=	pos;	break;
			case (5):	pols[pols.length]			=	pos;	break;
			case (6):	platforms[platforms.length]	=	pos;	break;
			case (7):	agents[agents.length]		=	pos;	break;
		}
	}*/
}
////////////////////////////////////////////////////////////////////////////////
// InfoType::Save : Saves an infotype to a TinyXML node
void InfoType::Save(OrderData::InfoType info_type,TiXmlNode *node)
{
	TiXmlElement *type = new TiXmlElement("type");
	type->SetAttribute("name",m_name.c_str());
	type->SetAttribute("num",m_number);
	for(std::vector<InfoTypeEntry *>::iterator it=m_entries.begin();it!=m_entries.end();it++)
	{
		TiXmlElement *data= new TiXmlElement("data");
		data->SetAttribute("name",(*it)->m_name.c_str());
		data->SetAttribute("num",(*it)->m_number);
		data->SetAttribute("type",(*it)->m_type);
		data->LinkEndChild(new TiXmlText((*it)->m_data.c_str()));
		type->LinkEndChild(data);
	}
	node->LinkEndChild(type);
}
////////////////////////////////////////////////////////////////////////////////
// InfoTypeList::Save : Saves an infotype list to the file it was loaded from
void InfoTypeList::Save()
{
	TiXmlDocument doc;

	doc.LinkEndChild( new TiXmlDeclaration( "1.0", "iso-8859-1", "" ) ); // <?xml version="1.0" encoding="iso-8859-1" ?>
	TiXmlElement *data = new TiXmlElement("data");

	TiXmlElement *version = new TiXmlElement("version");
	version->LinkEndChild(new TiXmlText("Offline Order Editor"));
	data->LinkEndChild(version);	

	TiXmlElement *types = new TiXmlElement("data_types");

	for(std::map<OrderData::InfoType,InfoType*>::iterator it=m_types.begin();it!=m_types.end();it++)
	{
		(*it).second->Save((*it).first,types);
	}

	data->LinkEndChild(types);
	doc.LinkEndChild(data);
	doc.SaveFile(m_filename);
}
// Global infotype list!
InfoTypeList infotypes("info_data.xml");
