// codedialog.hpp : Header for CodeDialog

// CodeDialog : Dialog for entering a userid / code combo
class CodeDialog : public wxDialog
{
public:
	CodeDialog(wxWindow *w,int *userid_ptr,std::string *hashcode_ptr);
private:
	wxStaticText *m_userid_label;
	wxStaticText *m_hash_label;
	wxTextCtrl *m_userid_entry;
	wxTextCtrl *m_hash_entry;
	int *m_userid;
	std::string *m_hashcode;
	bool m_loaded;

	void IDChanged(wxCommandEvent &unused);
	void HashChanged(wxCommandEvent &unused);
};
