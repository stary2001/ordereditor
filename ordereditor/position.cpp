//position.cpp : Position and PositionList

#include "common.hpp"
#include "position.hpp"
////////////////////////////////////////////////////////////////////////////////
//Constructor
PositionList::PositionList(std::string filename)
{
	TiXmlDocument doc;

	if(!doc.LoadFile(filename))
	{
		Util::ShowConsole();
		std::cout << doc.ErrorDesc() << std::endl;
		if(doc.ErrorRow() !=0 && doc.ErrorCol()!=0)
		{
			std::cout << "line " << doc.ErrorRow() << ", column " << doc.ErrorCol() << std::endl;
		}
		//MessageBoxA(NULL,(filename + " cannot be loaded.").c_str(),"Error",MB_OK);
		m_loaded = false;
	}
	else
	{
		TiXmlNode *data = doc.FirstChild("data");
		TiXmlNode *positions = data->FirstChild("positions");

		TiXmlNode *child=NULL;

		for(;child=positions->IterateChildren(child);)
		{
			int num = atoi(static_cast<TiXmlElement*>(child)->Attribute("num"));
			std::string name = static_cast<TiXmlElement*>(child)->Attribute("name");
			std::string system = child->FirstChild("system_text")->FirstChild()->Value();
			std::string loctext = child->FirstChild("loc_text")->FirstChild()->Value();
			std::string size = child->FirstChild("size")->FirstChild()->Value();
			std::string design = child->FirstChild("design")->FirstChild()->Value();
			std::string cl = child->FirstChild("class")->FirstChild()->Value();

			Position *p= new Position(num,name,system,loctext,size,design,cl);
			m_positions.push_back(p);
		}
		m_loaded = true;
	}
}
////////////////////////////////////////////////////////////////////////////////
//destructor
PositionList::~PositionList()
{
	for(std::vector<Position *>::iterator it=m_positions.begin();it!=m_positions.end();it++)
	{
		delete (*it);
	}
}

// Global position list!
PositionList positions("pos_list.xml");
