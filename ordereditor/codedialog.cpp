// codedialog.cpp : CodeDialog class

#include "common.hpp"
#include "codedialog.hpp"
////////////////////////////////////////////////////////////////////////////////
// Constructor
CodeDialog::CodeDialog(wxWindow *w,int *userid_ptr,std::string *hashcode_ptr) : wxDialog(w,wxID_ANY,"Enter Code"), m_userid_entry(0)
{
	m_loaded = false;
	m_userid_label = new wxStaticText(this,wxID_ANY,"User ID",wxPoint(8,36),wxSize(64,24));
	m_hash_label = new wxStaticText(this,wxID_ANY,"Code",wxPoint(8,68),wxSize(64,24));
	int id = idman.NextID();
	Connect(id,wxEVT_COMMAND_TEXT_UPDATED,wxCommandEventHandler(CodeDialog::IDChanged));

	std::ostringstream os;
	os << *userid_ptr;
	m_userid_entry = new wxTextCtrl(this,id,os.str().c_str(),wxPoint(64,32),wxSize(128,24));
	m_userid_entry->ChangeValue(config["userid"].c_str());

	id = idman.NextID();
	Connect(id,wxEVT_COMMAND_TEXT_UPDATED,wxCommandEventHandler(CodeDialog::HashChanged));
	std::string hash = *hashcode_ptr;
	const char *str = hash.c_str();

	m_hash_entry = new wxTextCtrl(this,id,str,wxPoint(64,64),wxSize(128,24));
	m_hash_entry->ChangeValue(config["hash"].c_str());

	m_userid = userid_ptr;
	m_hashcode = hashcode_ptr;
	m_loaded = true;
}
////////////////////////////////////////////////////////////////////////////////
// CodeDialog::IDChanged : Gets called when the ID textbox changes
void CodeDialog::IDChanged(wxCommandEvent &unused)
{
	if (m_userid_entry && m_userid && m_loaded)
	{
		wxString str = m_userid_entry->GetValue();
		const char *c=str.c_str();
		int i = atoi(c);
		*m_userid = i;
	}
}
////////////////////////////////////////////////////////////////////////////////
// CodeDialog::HashChanged : Gets called when the code textbox changes
void CodeDialog::HashChanged(wxCommandEvent &unused)
{
	if (m_hash_entry && m_hashcode && m_loaded)
	{
		*m_hashcode = m_hash_entry->GetValue().c_str();
	}
}
