// sentturns.hpp : Header for SentTurnsDialog

// SentTurnsDialog : Dialog for managing sent turns
class SentTurnsDialog : public wxDialog
{
public:
	SentTurnsDialog(wxWindow *w);
	~SentTurnsDialog();
private:
	std::vector<TurnList *> m_turn_list_list; // yep, it's a list of lists
	
	wxButton *m_restore;
	wxTreeCtrl *m_tree;
	void OnRestore(wxCommandEvent &unused);
};