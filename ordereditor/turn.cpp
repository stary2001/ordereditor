////turn.cpp : Turn and TurnList
#include "common.hpp"

#include "order.hpp"
#include "infotypes.hpp"
#include "position.hpp"
#include "turn.hpp"
#include "panel.hpp"

extern Panel *main_panel;
////////////////////////////////////////////////////////////////////////////////
// readOrderData : Makes an OrderData from a data node, a type, an infotype and a name
OrderData *readOrderData(TiXmlElement *data,OrderData::Type type,OrderData::InfoType infotype,std::string name)
{
	if(!data->NoChildren())
	{
		return new OrderData(type,infotype,data->FirstChild()->Value(),""); // contents of <data> tags as an int
	}
	else
	{
		return new OrderData(type,infotype,"",name);
	}
}
////////////////////////////////////////////////////////////////////////////////
//copy constructor
Turn::Turn(Turn &other)
{
	m_version=other.m_version;
	m_seqorder=other.m_seqorder;
	m_posid=other.m_posid;
	m_send=other.m_send;
	m_sequenced=other.m_sequenced;
	m_saved=other.m_saved;
	m_id=other.m_maxid++;
	m_filename=other.m_filename;
	m_posid=other.m_posid;
	m_posname=other.m_posname;
	m_posflag=other.m_posflag;

	std::vector<Order*> orders;
	for(std::vector<Order*>::iterator it = other.m_orders.begin();it!=other.m_orders.end();it++)
	{
		Order *neworder = (*it)->Clone();
		orders.push_back(neworder);
	}
	m_orders=orders;
}
////////////////////////////////////////////////////////////////////////////////
//Constructor
Turn::Turn(std::string filename)
{
	m_send=true;
	m_sequenced=false;
	m_saved=false;
	m_id=m_maxid++;
	m_filename=filename;

	if(!m_doc.LoadFile(m_filename.c_str()))
	{
		std::cout << m_doc.ErrorDesc() << std::endl;
		if(m_doc.ErrorRow() !=0 && m_doc.ErrorCol()!=0)
		{
			std::cout << "line " << m_doc.ErrorRow() << ", column " << m_doc.ErrorCol() << std::endl;
		}

		//MessageBoxA(NULL,(filename + " cannot be loaded.").c_str(),"Error",MB_OK);
		//exit(1);
		return;
	}
	//TiXmlElement *root=m_doc.RootElement();

	TiXmlNode *data = m_doc.FirstChild("data");
	Load(data);
}
////////////////////////////////////////////////////////////////////////////////
//Turn::Load : Loads a turn from a TinyXML node
void Turn::Load(TiXmlNode *data)
{
	bool template_turn=true;
	
	if(data->FirstChild("postype")) // assume turns with no postype set are templates
	{
		if(!(template_orders.m_loaded && infotypes.m_loaded && positions.m_loaded)) // don't load if we are missing data!
		{
			m_loaded = false;
			return;
		}

		TiXmlNode *postype_elem = data->FirstChild("postype");
		const char *postype=postype_elem->FirstChild()->Value();
		m_posflag=static_cast<Order::PosType>(atoi(postype));
	
		TiXmlNode *posid_elem = data->FirstChild("posid");
		const char *posid=posid_elem->FirstChild()->Value();
		m_posid=atoi(posid);
		
		TiXmlNode *send_elem = data->FirstChild("send");
		std::string send=send_elem->FirstChild()->Value();
		m_send = (send == "true"); 

		TiXmlNode *sequenced_elem = data->FirstChild("sequenced");
		std::string sequenced=sequenced_elem->FirstChild()->Value();
		m_sequenced = (sequenced == "true"); 

		const char *seqorder=data->FirstChild("seqorder")->FirstChild()->Value();
		m_seqorder=atoi(seqorder);
		
		TiXmlNode *sent_elem = data->FirstChild("sent");
		if(sent_elem)
		{
			std::string sent=sent_elem->FirstChild()->Value();
			m_sent = (sent == "true");
		}

		TiXmlNode *posname_elem = data->FirstChild("posname");
		if(posname_elem)
		{
			TiXmlNode *posname_text = posname_elem->FirstChild(); 
			
			if(posname_text)
			{
				m_posname=posname_text->Value();
			}
		}

		template_turn=false;
	}

	TiXmlNode *version=data->FirstChild("version")->FirstChild();

	m_version= atof(version->Value());
	TiXmlNode *orders=data->FirstChild("orders");
	
	TiXmlNode *child=NULL;
	for(;child=orders->IterateChildren(child);)
	{
		if(template_turn)
		{
			TiXmlElement *e=static_cast<TiXmlElement*>(child);
			TiXmlElement *d=static_cast<TiXmlElement*>(e->FirstChild("params"));

			const char *name=e->Attribute("name");
			const char *desc;
			
			if((!e->NoChildren())&&e->FirstChild("desc"))
			{
				TiXmlNode *n=e->FirstChild("desc");
				if(n->FirstChild())
				{
					desc=n->FirstChild()->Value();
				}
				else
				{
					desc="";
				}
			}
			else
			{
				desc="";
			}
			int id=atoi(e->Attribute("id"));
			int typeflag=atoi(e->Attribute("typeflag"));
			int posflag=atoi(e->Attribute("posflag"));

			std::vector<OrderData *> data;
			TiXmlNode *data_node=NULL;

			for(;data_node=d->IterateChildren(data_node);)
			{
				TiXmlElement *dn=static_cast<TiXmlElement*>(data_node);
				OrderData::Type dtype=static_cast<OrderData::Type>(atoi(dn->Attribute("datatype")));
				OrderData::InfoType itype=static_cast<OrderData::InfoType>(atoi(dn->Attribute("infotype")));
				const char *name=dn->Attribute("name");
				data.push_back(readOrderData(dn,dtype,itype,name));
			}
			

			TiXmlNode *link_node=NULL;
			std::vector<std::pair<int,int> > links;

			for(;link_node=e->IterateChildren("links",link_node);)
			{
				TiXmlElement *elem=reinterpret_cast<TiXmlElement*>(link_node);
				int itype=atoi(elem->Attribute("info_type"));
				int itype_linked=atoi(elem->Attribute("linked_info_type"));
				links.push_back(std::make_pair(itype,itype_linked));
			}	

			Order *o=new Order(id,name,data,static_cast<Order::Type>(typeflag),static_cast<Order::PosType>(posflag),links);
			o->m_desc=desc;
			m_orders.push_back(o);
		}
		else
		{
			TiXmlElement *e=static_cast<TiXmlElement*>(child);
			TiXmlElement *d=static_cast<TiXmlElement*>(e->FirstChild("params"));
			int id=atoi(e->Attribute("id"));
	
			Order *torder=template_orders.FindID(id);
			if(torder)
			{
				std::vector<OrderData *> data;
				TiXmlNode *data_node=NULL;
				int i=0;
				if(d)
				{
					for(;data_node=d->IterateChildren(data_node);)
					{
						TiXmlElement *dn=static_cast<TiXmlElement*>(data_node);
						OrderData::Type dtype=torder->m_data[i]->m_type;
						OrderData::InfoType itype=torder->m_data[i]->m_infotype;
					
						data.push_back(readOrderData(dn,dtype,itype,""));
						i++;
					}
				}
				std::vector<std::pair<int,int> > v;
				m_orders.push_back(new Order(id,torder->m_name,data,torder->m_typeflag,torder->m_posflag,v));
			}
		}	
			
	}
	m_loaded = true;
}
////////////////////////////////////////////////////////////////////////////////
//Destructor
Turn::~Turn()
{
	Save(m_filename);
	for(std::vector<Order*>::iterator it=m_orders.begin();it!=m_orders.end();it++)
	{
		delete (*it);
	}
}

////////////////////////////////////////////////////////////////////////////////
//Turn::FindID : Finds an order with ID id in this turn
Order *Turn::FindID(int id)
{
	for(std::vector<Order*>::iterator it=m_orders.begin();it!=m_orders.end();it++)
	{
		if((*it)->m_id==id) { return (*it); }
	}
	return NULL;
}

////////////////////////////////////////////////////////////////////////////////
//Turn::Save : Saves this turn to a TinyXML node
void Turn::Save(TiXmlNode *turn) // assume appending
{
	std::ostringstream s;

	TiXmlElement *version = new TiXmlElement( "version" );
	version->LinkEndChild(new TiXmlText("Offline Order Editor"));
	turn->LinkEndChild(version); // <version>0.1</version>

	TiXmlElement *postype = new TiXmlElement( "postype" );

	s << m_posflag;
	postype->LinkEndChild(new TiXmlText(s.str().c_str()));
	turn->LinkEndChild(postype);

	TiXmlElement *posid = new TiXmlElement( "posid" );

	s.str("");
	s << m_posid;
	posid->LinkEndChild(new TiXmlText(s.str().c_str()));
	turn->LinkEndChild(posid);
	
	TiXmlElement *orderlist=new TiXmlElement("orders");

	for(std::vector<Order*>::iterator it=m_orders.begin();it!=m_orders.end();it++)
	{
		TiXmlElement *elem = new TiXmlElement( "order" );
		TiXmlElement *params = new TiXmlElement("params");
		int i=0;
		for(std::vector<OrderData*>::iterator data_it=(*it)->m_data.begin();data_it!=(*it)->m_data.end();data_it++)
		{
			Order *torder=template_orders.FindID((*it)->m_id);
			TiXmlElement *curparam= new TiXmlElement("param");

			curparam->SetAttribute("infotype", torder->m_data[i]->m_infotype);
			curparam->SetAttribute("datatype", torder->m_data[i]->m_type);
			if((*data_it)->m_str!="") 
			{ 
				curparam->LinkEndChild(new TiXmlText((*data_it)->m_str.c_str())); 
			}
			params->LinkEndChild(curparam);
			i++;
		}
		if(!params->NoChildren())
			elem->LinkEndChild(params);

		s.str("");
		s << (*it)->m_id ;
		elem->SetAttribute("id",s.str().c_str());
		orderlist->LinkEndChild(elem);
	}

	turn->LinkEndChild(orderlist);

	TiXmlElement *send=new TiXmlElement("send");
	if(m_send)
	{
		send->LinkEndChild(new TiXmlText("true"));
	}
	else
	{
		send->LinkEndChild(new TiXmlText("false"));
	}
	turn->LinkEndChild(send);

	TiXmlElement *seq=new TiXmlElement("sequenced");
	if(m_sequenced)
	{
		seq->LinkEndChild(new TiXmlText("true"));
	}
	else
	{
		seq->LinkEndChild(new TiXmlText("false"));
	}
	turn->LinkEndChild(seq);
	
	TiXmlElement *seqorder=new TiXmlElement("seqorder");
	s.str("");
	s << m_seqorder;
	seqorder->LinkEndChild(new TiXmlText(s.str().c_str()));

	turn->LinkEndChild(seqorder);
	
	TiXmlElement *sent=new TiXmlElement("sent");

	sent->LinkEndChild(new TiXmlText(m_sent ? "true" : "false"));
	turn->LinkEndChild(sent);


	TiXmlElement *posname=new TiXmlElement("posname");

	posname->LinkEndChild(new TiXmlText(m_posname));
	turn->LinkEndChild(posname);

	m_saved=true;
}

////////////////////////////////////////////////////////////////////////////////
//Turn::Save : Saves this turn to a file specified by filename
void Turn::Save(std::string filename,bool force)
{
	if(m_saved && !force) { return; }
	if(this==&template_orders) { return; }

	if(filename=="")
	{
		wxFileDialog fd(main_panel,"Save Order","",main_panel->m_curdir,"Turn files(*.trn)|*.trn",wxFD_SAVE);
		if(fd.ShowModal()==wxID_OK)
			Save(std::string(fd.GetPath().c_str()));
		return;
	}

	TiXmlDocument doc(filename.c_str());

	doc.LinkEndChild( new TiXmlDeclaration( "1.0", "iso-8859-1", "" ) ); // <?xml version="1.0" ?>
	TiXmlElement *turn = new TiXmlElement("turn");
	Save(turn);
	doc.LinkEndChild(turn);
	doc.SaveFile(filename);
}

////////////////////////////////////////////////////////////////////////////////
//Destructor
TurnList::~TurnList()
{
	Save();
	for(std::vector<Turn*>::iterator it=m_turns.begin();it!=m_turns.end();it++)
	{
		delete (*it);
	}
}

////////////////////////////////////////////////////////////////////////////////
//TurnList::Save : Saves the TurnList to the file it was loaded from
void TurnList::Save()
{
	TiXmlDocument doc;

	doc.LinkEndChild( new TiXmlDeclaration( "1.0", "iso-8859-1", "" ) ); // <?xml version="1.0" ?>
	TiXmlElement *data = new TiXmlElement("data");

	TiXmlElement *turns = new TiXmlElement("turns");

	for(std::vector<Turn*>::iterator it=m_turns.begin();it!=m_turns.end();it++)
	{
		TiXmlElement *turn = new TiXmlElement("turn");
		(*it)->Save(turn);
		turns->LinkEndChild(turn);
	}
	data->LinkEndChild(turns);

	std::ostringstream os;
	TiXmlElement *timestamp=new TiXmlElement("timestamp");
	os.str("");
	os << m_timestamp;
	timestamp->LinkEndChild(new TiXmlText(os.str().c_str()));
	data->LinkEndChild(timestamp);

	doc.LinkEndChild(data);
	doc.SaveFile(m_filename);
}

////////////////////////////////////////////////////////////////////////////////
//TurnList::Load : Loads the TurnList from the filename it was constructed with
void TurnList::Load()
{
	TiXmlDocument doc;
	if(!doc.LoadFile(m_filename))
	{
		//MessageBoxA(NULL,(m_filename + " cannot be loaded.").c_str(),"Error",MB_OK);
		return;
	}

	TiXmlNode *data = doc.FirstChild("data");
	TiXmlNode *turns = data->FirstChild("turns");

	TiXmlNode *child = NULL;
	for(;child=turns->IterateChildren(child);)
	{
		Turn *t = new Turn();
		t->Load(child);
		m_turns.push_back(t);
	}

	TiXmlNode *timestamp_elem = data->FirstChild("timestamp");
	if(timestamp_elem)
	{
		std::string timestamp=timestamp_elem->FirstChild()->Value();
		std::istringstream is(timestamp);
		is >> m_timestamp;
	}
}

//Turn ID
int Turn::m_maxid=0;
// Global template orders list
Turn template_orders("order_data.xml");
