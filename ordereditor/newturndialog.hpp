// newturndialog.hpp : NewTurnDialog class

// NewTurnDialog - New Turn dialog class
class NewTurnDialog : public wxDialog
{
public:

	NewTurnDialog(wxWindow *w): wxDialog(w,wxID_ANY,"New Turn")
	{
		wxString types[]={"Ground Party","Ship","Starbase/Outpost","Political","Agent","Debris"};

		m_turntypelabel=new wxStaticText(this,wxID_ANY,"Turn Type",wxPoint(50,32),wxSize(64,24));

		m_turntype=new wxChoice(this,wxID_ANY,wxPoint(120,32),wxSize(130,24),6,types);

		int id=idman.NextID();

		Connect(id, wxEVT_COMMAND_BUTTON_CLICKED ,wxCommandEventHandler(NewTurnDialog::Create));
		m_create=new wxButton(this,id,"Create",wxPoint(32,128),wxSize(128,32));


		m_pos_textinput=new wxTextCtrl(this,wxID_ANY,"",wxPoint(50,64),wxSize(64,24));

		id=idman.NextID();
		Connect(id, wxEVT_COMMAND_CHOICE_SELECTED ,wxCommandEventHandler(NewTurnDialog::PosSelected));
		m_pos_choice=new wxChoice(this,id,wxPoint(120,64),wxSize(128,24));
		FillList();
	}


	void FillList();
	void PosSelected(wxCommandEvent &unused);

	wxStaticText *m_turntypelabel;
	wxChoice *m_turntype;

	wxButton *m_create;

	wxTextCtrl *m_pos_textinput;
	wxChoice *m_pos_choice;

	TurnDialog *dlg;

	Turn *m_turn;

	void Create(wxCommandEvent &unused);
};