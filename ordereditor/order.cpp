// order.cpp : Order related classes
#include "common.hpp"

#include "order.hpp"
///////////////////////////////////////////////////////////////////////////////
//Order::operator << : Allows debug output
std::ostream & Order::operator << (std::ostream &o)
{
	o << "ID: " << m_id << " name: " << m_name << std::endl;
	return o;
}
///////////////////////////////////////////////////////////////////////////////
//Destructor
Order::~Order()
{
	std::vector<OrderData*>::iterator it=m_data.begin();
	for(;it!=m_data.end();it++)
	{
		delete (*it);
	}
}
///////////////////////////////////////////////////////////////////////////////
//Order::Clone : Clones an order
Order *Order::Clone()
{
	std::vector<OrderData*> data;
	for(std::vector<OrderData*>::iterator it = m_data.begin();it!=m_data.end();it++)
	{
		OrderData *odata = new OrderData((*it)->m_type,(*it)->m_infotype,(*it)->m_str,(*it)->m_name);
		odata->m_proxy_infotype = (*it)->m_proxy_infotype;
		data.push_back(odata);
	}
	std::vector<std::pair<int,int> > v;
	return new Order(m_id,m_name,data,m_typeflag,m_posflag,v);
}
