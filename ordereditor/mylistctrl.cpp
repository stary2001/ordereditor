//mylistctrl.cpp : Quick override of wxListCtrl to pass mouse events to the panel

#include "common.hpp"
#include "panel.hpp"
#include "mylistctrl.hpp"
#include "order.hpp"
#include "turn.hpp"
#include "position.hpp"
////////////////////////////////////////////////////////////////////////////////
// MyListCtrl::OnClick : Handles click events
void MyListCtrl::OnClick(wxMouseEvent &ev)
{
	m_panel->OnClick(ev);
}

BEGIN_EVENT_TABLE(MyListCtrl,wxListCtrl)
	EVT_LEFT_DOWN(MyListCtrl::OnClick)
END_EVENT_TABLE()