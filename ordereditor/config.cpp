// config.cpp : config class
#include "common.hpp"
////////////////////////////////////////////////////////////////////////////////
//Constructor
Config::Config(std::string filename)
{
	m_filename = filename;
	TiXmlDocument doc;

	if(!doc.LoadFile(filename))
	{
		Util::ShowConsole();
		std::cout << doc.ErrorDesc() << std::endl;
		if(doc.ErrorRow() !=0 && doc.ErrorCol()!=0)
		{
			std::cout << "line " << doc.ErrorRow() << ", column " << doc.ErrorCol() << std::endl;
		}
		//MessageBoxA(NULL,(filename + " cannot be loaded.").c_str(),"Error",MB_OK);
		//exit(1);
	}
	
	TiXmlNode *data = doc.FirstChild("data");

	TiXmlNode *child = NULL;

	for(;child = data->IterateChildren(child);)
	{
		if(child->FirstChild()) // is it empty?
		{
			m_values[child->Value()] = child->FirstChild()->Value(); // <name>option</name> -> m_values[name]=option
		}
		else
		{
			m_values[child->Value()] = "";
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
// Config::Save() : Saves current config
void Config::Save()
{
	TiXmlDocument doc;
	doc.LinkEndChild( new TiXmlDeclaration( "1.0", "iso-8859-1", "" ) );
	TiXmlElement *data = new TiXmlElement("data");
	
	for(std::map<std::string,std::string>::iterator it =m_values.begin();it!=m_values.end();it++)
	{
		TiXmlElement *el=new TiXmlElement((*it).first.c_str());
		el->LinkEndChild(new TiXmlText((*it).second.c_str()));
		data->LinkEndChild(el);
	}

	doc.LinkEndChild(data);
	doc.SaveFile(m_filename);
}
////////////////////////////////////////////////////////////////////////////////
// Index operator, get a value
std::string &Config::operator[](std::string k)
{
	return m_values[k];
}

Config config("config.xml");
