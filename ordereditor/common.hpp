// common.hpp : common header, included in every file
#pragma warning(disable: 4996) // turn off "was declared deprecated" warnings

// standard library includes
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <sstream>

// include wxwidgets
#include <wx/wx.h>
#include <wx/listbox.h>
#include <wx/listctrl.h>
#include <wx/menu.h>
#include <wx/renderer.h>
#include <wx/treectrl.h>
#include <wx/imaglist.h>

// include TinyXML
#define TIXML_USE_STL
#include "tinyxml.h"

//include cURLpp
#define CURL_STATICLIB
#define CURLPP_STATICLIB
#include "curlpp/cURLpp.hpp"
#include "curlpp/Easy.hpp"
#include "curlpp/Options.hpp"
using curlpp::options::Url;

//include boost::filesystem and boost::date_time
#include "boost/filesystem.hpp"
using namespace boost::filesystem;
#include "boost/date_time.hpp"
using namespace boost::posix_time;

extern bool init;

// "common" headers

#include "idmanager.hpp"
#include "config.hpp"
#include "util.hpp"
