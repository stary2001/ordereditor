// util.hpp : utility functions,etc

namespace Util
{
	std::string GetFileNameFromPath(std::string path);
	extern void ShowConsole();
	extern bool PtrIsValid(void *ptr);
	extern void Explode(std::vector<std::string> &v,std::string s,std::string sep);
}