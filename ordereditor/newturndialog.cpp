//newturndialog.cpp : NewTurnDialog class

#include "common.hpp"
#include "turndialog.hpp"
#include "infotypes.hpp"
#include "position.hpp"
#include "newturndialog.hpp"

////////////////////////////////////////////////////////////////////////////////
//IndexToPosType : Converts a list index to a PosType
Order::PosType IndexToPosType(int index)
{
	switch(index)
	{
	case 0:
		return Order::Group; // ?
	break;

	case 1:
		return Order::Ship;
	break;

	case 2:
		return Order::Starbase;
	break;

	case 3:
		return Order::Political;
	break;

	case 4:
		return Order::Agent;
	break;

	case 5:
		return Order::Debris;
	break;

	default:
		return Order::None;
	break;

	}
}
////////////////////////////////////////////////////////////////////////////////
// NewTurnDialog::FillList : Fills the position list
void NewTurnDialog::FillList()
{
	std::ostringstream os;
	
	std::vector<wxString> strings;
	std::vector<void *> types;

	std::vector<Position*>& v=positions.m_positions;
	for(std::vector<Position*>::iterator it=v.begin();it!=v.end();it++)
	{
		std::string s=(*it)->m_name;
		s+=" (";
		os.str("");
		os << (*it)->m_number;
		s+=os.str();
		s+=")";
		strings.push_back(s.c_str());
		types.push_back((*it));
	}
	if(strings.size()!=0)
	{
		m_pos_choice->Append(strings.size(),&strings[0],&types[0]);
	}
}

////////////////////////////////////////////////////////////////////////////////
// NewTurnDialog::PosSelected : is called when a position is selected
void NewTurnDialog::PosSelected(wxCommandEvent &unused)
{
	std::ostringstream os;
	if(m_pos_choice->GetCurrentSelection()!=-1)
		os << static_cast<Position*>(m_pos_choice->GetClientData(m_pos_choice->GetCurrentSelection()))->m_number;

	m_pos_textinput->SetValue(os.str().c_str());
}

////////////////////////////////////////////////////////////////////////////////
// NewTurnDialog::Create : is called when a position is selected
void NewTurnDialog::Create(wxCommandEvent &unused)
{
	dlg=new TurnDialog(this,IndexToPosType(m_turntype->GetCurrentSelection()));
	dlg->m_turn->m_posid=atoi(m_pos_textinput->GetValue().c_str());
	dlg->ShowModal();
	m_turn=dlg->m_turn;
	EndModal(wxOK);
}
////////////////////////////////////////////////////////////////////////////////